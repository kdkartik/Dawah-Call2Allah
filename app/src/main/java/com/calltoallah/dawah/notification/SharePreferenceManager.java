package com.calltoallah.dawah.notification;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharePreferenceManager {
    private static SharePreferenceManager INSTANCE = null;
    private static final String APPINFO = "app_info";
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    public interface CONST {
        String NOTIFICATION_TIMES_ARRAY = "NOTIFICATION_TIMES_ARRAY";
        String RE_GENERATE_TIME = "reGenerateTime";
        String RE_GENERATE_ID = "reGenerateTimeId";
        String RE_GENERATE_NAMAZ_TIME = "reGenerateNamazTime";
        String UPCOMING_ID = "upcomingId";
        String IS_MUTE = "isMute";
    }

    private SharePreferenceManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(APPINFO, MODE_PRIVATE);
    }

    public static SharePreferenceManager getInstance(Context context) {
        if(INSTANCE == null)
            INSTANCE = new SharePreferenceManager(context);
        return INSTANCE;
    }

    public SharedPreferences getSharePreference() {
        return mSharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        if (mEditor == null) {
            mEditor = mSharedPreferences.edit();
        }
        return mEditor;
    }
}
