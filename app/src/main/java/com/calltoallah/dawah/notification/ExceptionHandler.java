package com.calltoallah.dawah.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final Context myContext;

    public ExceptionHandler(Context context) {
        myContext = context;
    }

    @Override
    public void uncaughtException(Thread t, Throwable exception) {
        final StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);

        exception.printStackTrace();
        LogTag.d("Exception : " + stackTrace);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);

        if (AlarmService.isInstanceCreated()) {
            LogTag.d("Exception Stop : AlarmService");
            myContext.stopService(new Intent(myContext, AlarmService.class));
        } else {
            LogTag.d("Exception start : AlarmService");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                myContext.startForegroundService(new Intent(myContext, AlarmService.class));
            } else {
                myContext.startService(new Intent(myContext, AlarmService.class));
            }
        }
    }
}
