package com.calltoallah.dawah.notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.util.HashSet;
import java.util.Set;

public class Restarter extends BroadcastReceiver {
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Broadcast Listened", "Service tried to stop");
        if(intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
            LogTag.d(intent.getAction());
            return;
        } else if(intent.getAction().equals(Intent.ACTION_DATE_CHANGED)) {
            LogTag.d(intent.getAction());
            return;
        }

        if (AlarmService.isInstanceCreated()) {
            context.stopService(new Intent(context, AlarmService.class));
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, AlarmService.class));
            } else {
                context.startService(new Intent(context, AlarmService.class));
            }
        }
        /**/
    }
}