package com.calltoallah.dawah.notification.database;

import androidx.room.Room;

import com.calltoallah.dawah.comman.Constants;

import java.util.List;

public final class DbManager {
    private static final DbManager INSTANCE = new DbManager();
    private static final String DB_NAME = "namaz_table";
    private static AppDatabase sDb;

    private DbManager() {
        /*final Migration MIGRATION_2_3 = new Migration(2, 3) {
            @Override
            public void migrate(SupportSQLiteDatabase database) {
                // Create the new table

                String sql = "CREATE TABLE IF NOT EXISTS `trackingMaster` (`uid` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `lat` TEXT, `lng` TEXT, `provider` TEXT, `time` INTEGER NOT NULL)";
                database.execSQL(sql);

            }
        };*/
        sDb = Room.databaseBuilder(Constants.sContext,
                AppDatabase.class, DB_NAME)
//                .addMigrations(MIGRATION_2_3)
                .build();

    }

    public static DbManager getInstance() {
        return INSTANCE;
    }

    public List<MasjidMaster> getAllMasjid(){
        return sDb.masjidDao().getAll();
    }

    public List<NamazMaster> getAllNamaz(){
        return sDb.namazDao().getAll();
    }

    public long insertMasjid(MasjidMaster data){
        return sDb.masjidDao().addData(data);
    }

    public long insertNamaz(NamazMaster data){
        return sDb.namazDao().addData(data);
    }

    public MasjidMaster getMasjidFromLocation(double lat, double lng) {
        return sDb.masjidDao().getMasjidFromLocation(lat, lng);
    }

    public MasjidMaster getMasjidFromId(long id) {
        return sDb.masjidDao().getMasjidFromId(id);
    }

    public NamazMaster getNamazFromId(long id) {
        return sDb.namazDao().getNamazFromId(id);
    }

    public void deleteAllMasjid() {
        sDb.masjidDao().deleteData();
    }

    public void deleteAllNamaz() {
        sDb.namazDao().deleteData();
    }

    public List<NamazMaster> getAllNamaz(long currentTime){
        return sDb.namazDao().getAll(currentTime);
    }

    public void updateNamazMaster(NamazMaster namazMaster) {
        sDb.namazDao().updateData(namazMaster);
    }
}
