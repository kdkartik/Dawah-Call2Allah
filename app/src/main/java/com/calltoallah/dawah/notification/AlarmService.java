package com.calltoallah.dawah.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.BuildConfig;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.LocationTrack;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.ErrorModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.notification.SharePreferenceManager.CONST;
import com.calltoallah.dawah.notification.database.DbManager;
import com.calltoallah.dawah.notification.database.MasjidMaster;
import com.calltoallah.dawah.notification.database.NamazMaster;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class AlarmService extends Service {

    private long nextNotificationTime = 0;
    private Thread notificationThread = null, mainThread = null;
    private static AlarmService instance = null;
    private String ThreadId = "";
    private boolean responseFailed = false;
    private Ringtone ringtone;
    private PowerManager.WakeLock wakeLock = null;

    public static boolean isInstanceCreated() {
        return instance != null;
    }

    /**
     * Foreground Notification to bind service
     */
    private void showForgroundServiceNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
            NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            String CHANNEL_ID = "AlarmService";
            CharSequence name = "Alarm Service";
            String Description = "Alarm Service";
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(false);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(false);
            mChannel.setShowBadge(false);
            mNotificationManager.createNotificationChannel(mChannel);

            notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);

            Intent intent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            notificationBuilder.setSmallIcon(R.drawable.ic_action_prayer)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_action_prayer))
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Dawah Service is running")
                    .setColor(Color.parseColor("#585A7B"))
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent);

            Notification notification = notificationBuilder.build();
            notification.defaults = 0;
            startForeground(105, notification);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showForgroundServiceNotification();
        String deviceDetail = getDeviceName() + "_" + Build.VERSION.SDK_INT + "(" + Build.VERSION.RELEASE + ") AppVersion:" + BuildConfig.VERSION_NAME;
        LogTag.d("Alarm Service Created -> " + deviceDetail);

        Constants.sContext = this;
        instance = this;
        PowerManager mgr = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
        wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK ,"Dawah::AlarmService");
        wakeLock.acquire();

        // Write crash report in log file.
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

        mainThread = new Thread(new Runnable() {
            @Override
            public void run() {

                List<NamazMaster> list = DbManager.getInstance().getAllNamaz(Calendar.getInstance().getTimeInMillis());
                if (list == null) {
                    // If list is null then main application set 1 min delay in thread
                    try {
                        Thread.sleep(60 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else if (list.size() == 0) {
                    try {
                        // If list is empty then main application set 1 min delay in thread
                        Thread.sleep(60 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    LogTag.d("Database Records");
                    for (NamazMaster namaz : list) {
                        LogTag.d(namaz.toString());
                    }
                    // If list is not empty then fetch first record from fetched list.
                    NamazMaster namazMaster = list.get(0);
                    MasjidMaster masjidMaster = DbManager.getInstance().getMasjidFromId(namazMaster.masjid_id);
                    // Fetch Masjid record from masjid id and send upcoming event
                    SharePreferenceManager.getInstance(AlarmService.this).getEditor().putLong(CONST.UPCOMING_ID, namazMaster.uid).commit();
//                    LogTag.d(masjidMaster.toString());
                    sendBroadcast(new Intent("com.upcoming_event").putExtra("namaz", namazMaster).putExtra("masjid", masjidMaster));
                    startNotificationThread(namazMaster);
                }

//
            }
        });
        mainThread.start();

    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        LogTag.d("AlarmService onTrimMemory level=" + level);
        if (level >= 15)
            onDestroy();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        return START_STICKY;
    }

    public void startNotificationThread(final NamazMaster namazMaster) {
        // Set namaz time in nextNotificationTime
        nextNotificationTime = namazMaster.namaz_time;
        if (nextNotificationTime != 0) {
            notificationThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (ThreadId.equalsIgnoreCase(Thread.currentThread().getId() + "")) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                        long current = Calendar.getInstance().getTimeInMillis();
                        // Fetch reGenerateTime from preference
                        long reGenerateTime = SharePreferenceManager.getInstance(AlarmService.this).getSharePreference().getLong(CONST.RE_GENERATE_TIME, 0);
                        if (reGenerateTime != 0) {
                            if (nextNotificationTime > reGenerateTime && reGenerateTime > current) {
                                nextNotificationTime = reGenerateTime;
                            }
                        }
                       // LogTag.d("run: " + dateFormat.format(nextNotificationTime) + " ThreadId: " + notificationThread.getId());

                        if (current >= nextNotificationTime &&
                                (current < nextNotificationTime + 30 * 1000)) {
                            LogTag.d("showNotification: " + dateFormat.format(nextNotificationTime));
                            long upcomingId = SharePreferenceManager.getInstance(AlarmService.this).getSharePreference().getLong(CONST.UPCOMING_ID, 0);
                            NamazMaster master = namazMaster;
                            if (upcomingId > 0) {
                                master = DbManager.getInstance().getNamazFromId(upcomingId);
                            } else {
                                LogTag.d("upcomingId before showDialog = " + upcomingId);
                            }
                            showDialog(master, reGenerateTime == nextNotificationTime);
                            List<NamazMaster> namazMasterList = DbManager.getInstance().getAllNamaz(Calendar.getInstance().getTimeInMillis());
                            if (namazMasterList == null) {
                                nextNotificationTime = 0;
                            } else if (namazMasterList.size() == 0) {
                                nextNotificationTime = 0;
                            } else {
                                NamazMaster namazMaster = namazMasterList.get(0);
                                nextNotificationTime = namazMaster.namaz_time;
                                SharePreferenceManager.getInstance(AlarmService.this).getEditor().putLong(CONST.UPCOMING_ID, namazMaster.uid).commit();
                                MasjidMaster masjidMaster = DbManager.getInstance().getMasjidFromId(namazMaster.masjid_id);
                                // Fetch Masjid record from masjid id and send upcoming event
                                LogTag.d(masjidMaster.toString());
                                sendBroadcast(new Intent("com.upcoming_event").putExtra("namaz", namazMaster).putExtra("masjid", masjidMaster));
                            }
                            if (nextNotificationTime == 0) {
                                LogTag.d("nextNotificationTime == 0");
                                break;
                            } else
                                LogTag.d("nextNotificationTime = " + dateFormat.format(nextNotificationTime) + " -- " + nextNotificationTime + " current: " + Calendar.getInstance().getTimeInMillis());
                        } else {
                            if (current >= nextNotificationTime &&
                                    (current > nextNotificationTime + 30 * 1000)) {
                                LogTag.d("ELSE");
                                List<NamazMaster> namazMasterList = DbManager.getInstance().getAllNamaz(Calendar.getInstance().getTimeInMillis());
                                if (namazMasterList == null) {
                                    nextNotificationTime = 0;
                                } else if (namazMasterList.size() == 0) {
                                    nextNotificationTime = 0;
                                } else {
                                    NamazMaster namazMaster = namazMasterList.get(0);
                                    nextNotificationTime = namazMaster.namaz_time;
                                    SharePreferenceManager.getInstance(AlarmService.this).getEditor().putLong(CONST.UPCOMING_ID, namazMaster.uid).commit();
                                    MasjidMaster masjidMaster = DbManager.getInstance().getMasjidFromId(namazMaster.masjid_id);
                                    // Fetch Masjid record from masjid id and send upcoming event
                                    LogTag.d(masjidMaster.toString());
                                    sendBroadcast(new Intent("com.upcoming_event").putExtra("namaz", namazMaster).putExtra("masjid", masjidMaster));
                                }
                            }
                        }

                        try {
                            long delay = nextNotificationTime - current;
                            LogTag.d("Delay:" + delay + " - Next Notification Time: " + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(nextNotificationTime));
                            if(delay <= 60 * 1000 * 1)
                                Thread.sleep(1 * 1000);
                            else if(delay <= 60 * 1000 * 2)
                                Thread.sleep(30 * 1000);
                            else
                                Thread.sleep(60 * 1000);
                        } catch (InterruptedException e) {
                            LogTag.d("Interrupted ThreadId:" + ((notificationThread != null) ? notificationThread.getId() : ""));
                            break;
                        }
                    }

                }
            });
            ThreadId = notificationThread.getId() + "";
            notificationThread.start();
        }
    }


    private void showDialog(final NamazMaster namazMaster, final boolean isRegenrate) {
        Calendar calendar = Calendar.getInstance();
        long reGenerateNotification = SharePreferenceManager.getInstance(this).getSharePreference().getLong(CONST.RE_GENERATE_TIME, 0);
        final long reGenerateId = SharePreferenceManager.getInstance(this).getSharePreference().getLong(CONST.RE_GENERATE_ID, 0);
        LogTag.d("handleMessage: " + reGenerateNotification + " regenerate=" + isRegenrate);
        if (reGenerateNotification != 0 && isRegenrate) {
            LogTag.d("handleMessage: " + Math.abs(calendar.getTimeInMillis() - reGenerateNotification));
            if (Math.abs(calendar.getTimeInMillis() - reGenerateNotification) <= 30 * 1000) {
                LogTag.d("handleMessage: Clear");
                SharePreferenceManager.getInstance(this).getEditor().remove(CONST.RE_GENERATE_TIME).commit();
                SharePreferenceManager.getInstance(this).getEditor().remove(CONST.RE_GENERATE_NAMAZ_TIME).commit();
            }
        }
        boolean isMute = SharePreferenceManager.getInstance(this).getSharePreference().getBoolean(CONST.IS_MUTE, false);
        /**
         * Start below block if isMute not display notification status bar
         */

        if (isMute) {
            LogTag.d("isMute is On with no notification feature");
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                NamazMaster namazMaster1 = namazMaster;
                MasjidMaster masjidMaster = DbManager.getInstance().getMasjidFromId(namazMaster.masjid_id);
                if (masjidMaster != null) {
                    LogTag.d(masjidMaster.toString());
                    if (isRegenrate) {
                        LogTag.d("Show Dialog for Snooze");
                        namazMaster1 = DbManager.getInstance().getNamazFromId(reGenerateId);
                    }
                    namazMaster1.masjidMaster = masjidMaster;
                    handler.obtainMessage(101, namazMaster1).sendToTarget();
                }
            }
        }).start();

    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == 101) {
                final NamazMaster namazMaster = (NamazMaster) msg.obj;
                LogTag.d("Show Dialog =" + namazMaster.toString() + " --> " + namazMaster.masjidMaster.toString());

                boolean isMute = SharePreferenceManager.getInstance(AlarmService.this).getSharePreference().getBoolean(CONST.IS_MUTE, false);
                /**
                 * Start below block if isMute display notification without sound
                 */
                if (isMute == false) {
                    playNotificationSound(AlarmService.this);
                }

//                playNotificationSound(AlarmService.this);
                final View mFloatingView = LayoutInflater.from(AlarmService.this).inflate(R.layout.dialog_notification, null);

                //Add the view to the window.
                int windowFlags = WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS;

                int isPie;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    isPie = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
                } else {
                    isPie = WindowManager.LayoutParams.TYPE_PHONE;
                }

                final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        isPie, windowFlags,
                        PixelFormat.TRANSPARENT);


                //Add the view to the window
                final WindowManager mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                mWindowManager.addView(mFloatingView, params);

                //LogTag.d("Show Dialog =" + namazMaster.toString() + " --> " + namazMaster.masjidMaster.toString());

                TextView namazName = mFloatingView.findViewById(R.id.txtNamazName);
                namazName.setText("Its time for " + namazMaster.namaz_name + " Prayer");

                TextView txtMasjidName = mFloatingView.findViewById(R.id.txtMasjidName);
                txtMasjidName.setText(namazMaster.masjidMaster.name);

                TextView txtMasjidAddress = mFloatingView.findViewById(R.id.txtMasjidAddress);
                txtMasjidAddress.setText(namazMaster.masjidMaster.address);

                ImageView imgMasjidIcon = mFloatingView.findViewById(R.id.imgMasjidIcon);
                RequestOptions options = new RequestOptions()
                        .placeholder(R.drawable.no_profile_image)
                        .error(R.drawable.no_profile_image);

                Glide.with(getApplicationContext())
                        .load(API_DOMAIN + namazMaster.masjidMaster.image)
                        .apply(options)
                        .into(imgMasjidIcon);

                mFloatingView.findViewById(R.id.btnDirection).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + namazMaster.masjidMaster.latitude + "," +
                                namazMaster.masjidMaster.longitude);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mapIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        mapIntent.setPackage("com.google.android.apps.maps");
                        if (mapIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(mapIntent);
                        }
                        LogTag.d("Direction: ");
//                        mWindowManager.removeView(mFloatingView);
//                        SharePreferenceManager.getInstance(AlarmService.this).getEditor().commit();
                        NamazMaster data = (NamazMaster) v.getTag();
                        mWindowManager.removeView(mFloatingView);
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MINUTE, 2);

                        SharePreferenceManager.getInstance(AlarmService.this).getEditor()
                                .putLong(CONST.RE_GENERATE_TIME, calendar.getTimeInMillis()).commit();
                        SharePreferenceManager.getInstance(AlarmService.this).getEditor()
                                .putLong(CONST.RE_GENERATE_ID, data.uid).commit();
                        if (notificationThread != null) {
                            notificationThread.interrupt();
                            notificationThread = null;
                        }
                        startNotificationThread(data);
                    }
                });

                mFloatingView.findViewById(R.id.txtPRAY).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LogTag.d("Pray button Pressed");
                        if (ringtone != null)
                            ringtone.stop();
                        LocationTrack mLocationTrack = new LocationTrack(getApplicationContext());
                        if (mLocationTrack.canGetLocation()) {
                            String currentAddress = mLocationTrack.getCurrentAddress(mLocationTrack.getLatitude(), mLocationTrack.getLongitude());
                            addSalah(namazMaster.namaz_name, currentAddress);

                        } else {
                            mLocationTrack.showSettingsAlert();
                        }

                        mWindowManager.removeView(mFloatingView);
                        SharePreferenceManager.getInstance(AlarmService.this).getEditor().commit();
                    }
                });

                mFloatingView.findViewById(R.id.txtSKIP).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LogTag.d("Skip button Pressed");
                        if (ringtone != null)
                            ringtone.stop();
                        mWindowManager.removeView(mFloatingView);
                        SharePreferenceManager.getInstance(AlarmService.this).getEditor().commit();
                    }
                });
                mFloatingView.findViewById(R.id.txtREMIND).setTag(namazMaster);
                mFloatingView.findViewById(R.id.txtREMIND).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ringtone != null)
                            ringtone.stop();
                        NamazMaster data = (NamazMaster) view.getTag();
                        mWindowManager.removeView(mFloatingView);
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(Calendar.MINUTE, 2);
                        LogTag.d("Snoozing: " + calendar.getTimeInMillis() + " Namaz Id:" + data.uid + " data=" + data);
                        SharePreferenceManager.getInstance(AlarmService.this).getEditor()
                                .putLong(CONST.RE_GENERATE_TIME, calendar.getTimeInMillis()).commit();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                        SharePreferenceManager.getInstance(AlarmService.this).getEditor()
                                .putString(CONST.RE_GENERATE_NAMAZ_TIME, simpleDateFormat.format(new Date(data.namaz_time))).commit();
                        LogTag.d("Snoozing -> Namaz Timing -- " + simpleDateFormat.format(new Date(data.namaz_time)));
                        SharePreferenceManager.getInstance(AlarmService.this).getEditor()
                                .putLong(CONST.RE_GENERATE_ID, data.uid).commit();
                        if (notificationThread != null) {
                            notificationThread.interrupt();
                            notificationThread = null;
                        }
                        startNotificationThread(data);
                    }
                });

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Calendar calendar = Calendar.getInstance();
                        final long reGenerateId = SharePreferenceManager.getInstance(AlarmService.this).getSharePreference().getLong(CONST.RE_GENERATE_ID, 0);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        String today = simpleDateFormat.format(calendar.getTime());
                        calendar.setTimeInMillis(namazMaster.namaz_time);
                        String namaz = simpleDateFormat.format(calendar.getTime());
                        LogTag.d("Regenerate Id:- " + reGenerateId + " --> NamazId:-  " + namazMaster.uid + "\nToday:- " + today + " -- Namaz: " + namaz);
                        if (today.equals(namaz)) {
                            calendar.add(Calendar.DAY_OF_MONTH, 1);
                        }
                        namazMaster.namaz_time = calendar.getTimeInMillis();
                        DbManager.getInstance().updateNamazMaster(namazMaster);
                        LogTag.d(namazMaster.toString());
                    }
                }).start();
            }
        }
    };

    private void addSalah(String salah_name, String salah_address) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.addSalaah(UserPreferences.loadUser(getApplicationContext()).getId(),
                salah_name, salah_address, dateFormat.format(calendar.getTime()));
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ErrorModel errorModel = gson.fromJson(reader, ErrorModel.class);

                            if (errorModel.getStatus().equalsIgnoreCase("1")) {
                                Log.d("SalahOnSuccess: ", "" + errorModel.getMessage().toString());
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Log.d("SalahError: ", getString(R.string.unable_to_get_response));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.d("SalahError: ", getString(R.string.unable_to_get_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("SalahonFailure: ", getString(R.string.unable_to_get_response));
            }
        });
    }

    private SharedPreferences getSharedPreference() {
        return getSharedPreferences("Settings", MODE_PRIVATE);
    }


    public void playNotificationSound(Context context) {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + context.getPackageName() + "/raw/notification");
            ringtone = RingtoneManager.getRingtone(context, alarmSound);
            ringtone.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        LogTag.d("Alarm Service onTaskRemoved");
        stopSelf();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        LogTag.d("Alarm Service onDestroy");

        try {
            if(wakeLock != null) {
                wakeLock.release();
                wakeLock = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        instance = null;
        try {
            if (mainThread != null)
                mainThread.interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (notificationThread != null)
                notificationThread.interrupt();
        } catch (Exception e) {
            e.printStackTrace();
        }
        stopForeground(true);
        Intent in = new Intent(this, getClass());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(in);
        } else {
            startService(in);
        }
    }
}
