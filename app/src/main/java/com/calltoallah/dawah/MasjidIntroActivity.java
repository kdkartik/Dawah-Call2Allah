package com.calltoallah.dawah;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.calltoallah.dawah.adapter.MasjidIntroAdapter;
import com.calltoallah.dawah.comman.LocationTrack;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasjidIntroActivity extends MasterActivity {

    private boolean responseFailed = false;
    private ProgressbarManager progressbarManager;
    private RecyclerView rcvMasjidList;
    private MasjidIntroAdapter masjidAdapter;
    private double latitude, longitude;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LocationTrack mLocationTrack;
    public static Button btnNext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_masjid_intro);

        Init();

        mLocationTrack = new LocationTrack(this);
        if (mLocationTrack.canGetLocation()) {
            longitude = mLocationTrack.getLongitude();
            latitude = mLocationTrack.getLatitude();
            getMasjidList(latitude, longitude);
        } else {
            mLocationTrack.showSettingsAlert();
        }

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                getMasjidList(latitude, longitude);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MasjidIntroActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    private void getMasjidList(double lat, double lon) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getAllMasjidList(UserPreferences.loadUser(MasjidIntroActivity.this).getId(),
                String.valueOf(lat), String.valueOf(lon));
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                swipeRefreshLayout.setRefreshing(false);

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();
                        Log.d("MASJID: ", res);

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                            if (masjidModel.getMasjidListModel().size() > 0) {

                                List<MasjidListModel> mFilterList = new ArrayList<>();
                                for (MasjidListModel masjidListModel : masjidModel.getMasjidListModel()) {
                                    if (masjidListModel.getPost_count() > 0) {
                                        mFilterList.add(masjidListModel);
                                    }
                                }

                                if (mFilterList.size() > 0) {
                                    masjidAdapter = new MasjidIntroAdapter(MasjidIntroActivity.this, mFilterList);
                                    rcvMasjidList.setAdapter(masjidAdapter);
                                    masjidAdapter.notifyDataSetChanged();
                                }

                            } else {
                                Toast.makeText(MasjidIntroActivity.this, "Data not found!", Toast.LENGTH_SHORT).show();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(MasjidIntroActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(MasjidIntroActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbarManager.dismiss();
                Toast.makeText(MasjidIntroActivity.this, getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init() {
        progressbarManager = new ProgressbarManager(this);

        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        rcvMasjidList = findViewById(R.id.rcvMasjidList);
        btnNext = findViewById(R.id.btnNext);

        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        rcvMasjidList.setLayoutManager(manager);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLocationTrack.stopListener();
    }
}

