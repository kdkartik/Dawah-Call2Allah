package com.calltoallah.dawah.comman;

import android.app.Application;
import android.content.Context;

import com.calltoallah.dawah.MainActivity;

public class App extends Application {
    private static App mInstance;
    public static Context context;
    public static MainActivity mMain;
//    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public static MainActivity getContext() {
        return mMain;
    }

    public static void setContext(MainActivity mContext) {
        mMain = mContext;
    }

    public static synchronized App getInstance() {
        return mInstance;
    }
}