package com.calltoallah.dawah.comman;

import android.content.Context;

public class Constants {
    public final static String DATA = "DATA";
    public final static String OTHER = "OTHER";
    public final static String LOGIN = "LOGIN";
    public final static String USER_ID = "USER_ID";
    public final static String USERINFO = "USERINFO";
    public final static String ORDERID = "ORDERID";
    public final static String AMOUNT = "AMOUNT";
    public final static String MODE = "MODE";
    public final static String LATITUDE = "LATITUDE";
    public final static String LONGITUDE = "LONGITUDE";
    public final static String KEY_APP_DISCLOSURE = "app_disclosure";
    public static Context sContext;
}
