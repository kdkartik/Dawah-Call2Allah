package com.calltoallah.dawah.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.CalendarEventListModel;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {

    private Context context;
    private List<CalendarEventListModel> mList;

    public List<CalendarEventListModel> getListModel(int position) {
        return mList;
    }

    public List<CalendarEventListModel> getList() {
        return mList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView eventTitle, eventDate;

        public MyViewHolder(View view) {
            super(view);
            eventTitle = (TextView) view.findViewById(R.id.eventTitle);
            eventDate = (TextView) view.findViewById(R.id.eventDate);
        }
    }

    public EventAdapter(Context context, List<CalendarEventListModel> list) {
        this.context = context;
        this.mList = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_events, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.eventTitle.setText(mList.get(position).getEventName());
        holder.eventDate.setText(mList.get(position).getEventDate());
//        getMonth(mList.get(position).getEventMonth()) + " " + mList.get(position).getEventDay() + ", " + mList.get(position).getEventYear()
    }

//    private String getMonth(int event_month) {
//        String islamicMonth = "";
//        if (event_month == 0) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[0];
//        } else if (event_month == 1) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[1];
//        } else if (event_month == 2) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[2];
//        } else if (event_month == 3) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[3];
//        } else if (event_month == 4) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[4];
//        } else if (event_month == 5) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[5];
//        } else if (event_month == 6) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[6];
//        } else if (event_month == 7) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[7];
//        } else if (event_month == 8) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[8];
//        } else if (event_month == 9) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[9];
//        } else if (event_month == 10) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[10];
//        } else if (event_month == 11) {
//            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[11];
//        }
//        return islamicMonth;
//    }

//    public static String toTitleCase(String str) {
//        if (str == null) {
//            return null;
//        }
//        boolean space = true;
//        StringBuilder builder = new StringBuilder(str);
//        final int len = builder.length();
//
//        for (int i=0; i < len; ++i) {
//            char c = builder.charAt(i);
//            if (space) {
//                if (!Character.isWhitespace(c)) {
//                    // Convert to title case and switch out of whitespace mode.
//                    builder.setCharAt(i, Character.toTitleCase(c));
//                    space = false;
//                }
//            } else if (Character.isWhitespace(c)) {
//                space = true;
//            } else {
//                builder.setCharAt(i, Character.toLowerCase(c));
//            }
//        }
//
//        return builder.toString();
//    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}