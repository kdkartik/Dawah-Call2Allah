package com.calltoallah.dawah.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.QuranAdapter;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.QuranDataModel;
import com.calltoallah.dawah.model.QuranModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.calltoallah.dawah.MainActivity.mBottomNavigationView;
import static com.calltoallah.dawah.MasterActivity.mQuranList;

public class QuranFragment extends BaseFragment {

    private String TAG = QuranFragment.class.getSimpleName();

    private RecyclerView mSearchResult;
    private boolean responseFailed = false;
    private QuranAdapter quranAdapter;

    public static TextView txtSearchResult;
    private int REQUEST_VOICE = 13;
    private ProgressbarManager progressbarManager;
    private SearchView mSearchView;
    private String mSearchQuery = "";
    private LinearLayout errorLayout;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_quran, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();

        if (mQuranList.size() > 0) {
            quranAdapter = new QuranAdapter(activity, mQuranList);
            mSearchResult.setAdapter(quranAdapter);
            quranAdapter.notifyDataSetChanged();
//            searchDialog();
        } else {
            progressbarManager.show();
            requestQuran();
        }
    }

    private void setData() {
        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {
                if (quranAdapter != null) {
                    hideKeyboard();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSearchResult.setVisibility(View.VISIBLE);
                            errorLayout.setVisibility(View.GONE);
                            quranAdapter.getFilter().filter(query);
                            quranCount(query);
                        }
                    });
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                return false;
            }
        });


        ImageView clearButton = (ImageView) mSearchView.findViewById(R.id.search_close_btn);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchView.setQuery("", false);
                mSearchResult.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
                quranAdapter.getFilter().filter("");
                quranCount("");
                hideKeyboard();
            }
        });

        ImageView voiceButton = (ImageView) mSearchView.findViewById(R.id.search_voice_btn);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });
    }

//    private void searchDialog() {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_search);
//        dialog.setCancelable(false);
//        dialog.show();
//
//        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
//        mSearchView = (SearchView) dialog.findViewById(R.id.searchView);
//        Button btnSearch = (Button) dialog.findViewById(R.id.btnSearch);
//
//        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
//        mSearchView.setMaxWidth(Integer.MAX_VALUE);
//        mSearchView.setIconified(false);
//        mSearchView.clearFocus();
//
//        mSearchView.setQueryRefinementEnabled(true);
//        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                if (quranAdapter != null) {
//                    mSearchResult.setVisibility(View.VISIBLE);
//                    quranAdapter.getFilter().filter(query);
//                    quranCount(query);
//                    hideKeyboard();
//                }
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String query) {
//                mSearchQuery = query;
//                return false;
//            }
//        });
//
//
//        ImageView clearButton = (ImageView) mSearchView.findViewById(R.id.search_close_btn);
//        clearButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mSearchView.setQuery("", false);
//                mSearchView.clearFocus();
//                quranCount("");
//            }
//        });
//
//        ImageView voiceButton = (ImageView) mSearchView.findViewById(R.id.search_voice_btn);
//        voiceButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                askSpeechInput();
//            }
//        });
//
//        btnSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                if (mSearchQuery.equalsIgnoreCase("")) {
//                    Toast.makeText(activity, "" + getString(R.string.search_quran), Toast.LENGTH_SHORT).show();
//                } else {
//                    mSearchResult.setVisibility(View.VISIBLE);
//                    quranAdapter.getFilter().filter(mSearchQuery);
//                    quranCount(mSearchQuery);
//                    hideKeyboard();
//                }
//            }
//        });
//    }

    private void requestQuran() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.getQuran(UserPreferences.loadUser(getActivity()).getId());
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressbarManager.dismiss();

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            QuranModel quranModel = gson.fromJson(reader, QuranModel.class);
                            mQuranList = quranModel.getQuranDataModel();

                            if (mQuranList.size() > 0) {
                                errorLayout.setVisibility(View.VISIBLE);

                                quranAdapter = new QuranAdapter(activity, mQuranList);
                                mSearchResult.setAdapter(quranAdapter);
                                quranAdapter.notifyDataSetChanged();

                            } else {
                                errorLayout.setVisibility(View.VISIBLE);
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            errorLayout.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressbarManager.dismiss();
                errorLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void quranCount(CharSequence query) {
        if (query.equals("")) {
            txtSearchResult.setVisibility(View.GONE);

        } else {
            txtSearchResult.setVisibility(View.VISIBLE);
            List<QuranDataModel> filteredModelList = new ArrayList<>();
            for (QuranDataModel model : mQuranList) {
                final String text_english = model.getText_english().toLowerCase().toString();
                final String text_hindi = model.getText_hindi().toLowerCase().toString();
                final String text_urdu = model.getText_urdu().toLowerCase().toString();
                final String text_arabic = model.getText_arabic().toLowerCase().toString();

                if (text_english.contains(query) || text_hindi.contains(query) ||
                        text_urdu.contains(query) || text_arabic.contains(query)) {
                    filteredModelList.add(model);
                }
            }

            if (filteredModelList.size() > 0) {
                txtSearchResult.setText("'" + query + "'" + " found " + filteredModelList.size() + " times");
            } else {
                txtSearchResult.setText("'" + query + "'" + " not found ");
            }
        }

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Try saying something");
        try {
            startActivityForResult(intent, REQUEST_VOICE);
        } catch (ActivityNotFoundException a) {
            a.printStackTrace();
        }
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());
        errorLayout = v.findViewById(R.id.errorLayout);
        mSearchResult = v.findViewById(R.id.searchResult);
        txtSearchResult = v.findViewById(R.id.txtSearchResult);

        mSearchResult.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mSearchResult.setItemAnimator(new DefaultItemAnimator());

        SearchManager mSearchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) v.findViewById(R.id.searchView);

        mSearchView.setSearchableInfo(mSearchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setMaxWidth(Integer.MAX_VALUE);
        mSearchView.setIconified(false);
        mSearchView.clearFocus();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_VOICE && resultCode == RESULT_OK) {
            String query = data.getStringExtra(SearchManager.QUERY);
            mSearchView.setQuery(query, true);
            quranCount(query);
        }
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

