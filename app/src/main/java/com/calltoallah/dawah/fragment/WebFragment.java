package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;

import static com.calltoallah.dawah.MainActivity.mToolbarTitle;

public class WebFragment extends BaseFragment {

    private WebView webNews;
    private Bundle bundle;
    private String webLink;
    private ImageView web_loading;
    private int mode;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_web, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        bundle = getArguments();

        web_loading = (ImageView) v.findViewById(R.id.web_loading);
        webNews = (WebView) v.findViewById(R.id.webNews);
        webNews.setVerticalScrollBarEnabled(true);
        webNews.setHorizontalScrollBarEnabled(true);

        if (bundle != null) {
            mode = bundle.getInt(Constants.MODE);
            webLink = bundle.getString(Constants.DATA);
        }

        if (mode == 0) {
            mToolbarTitle.setText("Privacy Policy");
        } else if (mode == 1) {
            mToolbarTitle.setText("Terms & Services");
        } else {
            mToolbarTitle.setText("About US");
        }

        if (webLink.length() > 0) {
            webNews.getSettings().setJavaScriptEnabled(true);
            webNews.getSettings().setAllowFileAccess(true);
            webNews.getSettings().setDisplayZoomControls(false);

            webNews.getSettings().setPluginState(WebSettings.PluginState.ON);
            WebSettings webSettings = webNews.getSettings();
            webSettings.setBuiltInZoomControls(true);

            webNews.setWebViewClient(new Callback());  //HERE IS THE MAIN CHANGE
            webNews.loadUrl(webLink);
        }
    }

    private class Callback extends WebViewClient {  //HERE IS THE MAIN CHANGE.

        public Callback() {
            web_loading.setVisibility(View.VISIBLE);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            web_loading.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBack() {
        if (webNews.canGoBack()) {
            webNews.goBack();
        } else {
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
            activity.removeFragment(fragment);
        }
    }

}