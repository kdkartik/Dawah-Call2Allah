package com.calltoallah.dawah.fragment;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.HomeSelectedAdapter;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.HomeMasjidPostList;
import com.calltoallah.dawah.model.HomeModelMy;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.RecyclerViewItem;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.Gson;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostFragment extends BaseFragment {

    private boolean firstTime = true;
    private RecyclerView mRecyclerView;
    private HomeSelectedAdapter mAdapter;
    private MasjidListModel masjidListModel;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout errorLayout;
    private Button btnFollow;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_mypost, container, false);
    }

    public static MyPostFragment newInstance(MasjidListModel masjidListModel) {
        MyPostFragment aFragment = new MyPostFragment();
        Bundle aBundle = new Bundle();
        aBundle.putParcelable(Constants.DATA, masjidListModel);
        aFragment.setArguments(aBundle);
        return aFragment;
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            masjidListModel = getArguments().getParcelable(Constants.DATA);
        }

        Init(v);
        requestMedia();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                requestMedia();
            }
        });

        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.loadFragment(new MasjidFragment());
            }
        });
    }

    private void Init(View v) {
        errorLayout = (LinearLayout) v.findViewById(R.id.errorLayout);
        btnFollow = (Button) v.findViewById(R.id.btnFollow);
        swipeRefreshLayout = v.findViewById(R.id.swipeRefresh);

        mRecyclerView = v.findViewById(R.id.exoPlayerRecyclerView);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }

    private void requestMedia() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getUserAllPosts(UserPreferences.loadUser(getActivity()).getId(), masjidListModel.getId());
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                swipeRefreshLayout.setRefreshing(false);

                if (response.isSuccessful()) {
                    Log.d("USER_POST: ", response.body().toString());

                    Gson gson = new Gson();
                    Reader reader = new StringReader(response.body().toString());
                    HomeModelMy homeModelMy = gson.fromJson(reader, HomeModelMy.class);

                    if (homeModelMy.getHomeMasjidPostList().size() > 0) {
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        errorLayout.setVisibility(View.GONE);

                        List<HomeMasjidPostList> mFilterList = new ArrayList<>();
                        for (HomeMasjidPostList homeMasjidPostList : homeModelMy.getHomeMasjidPostList()) {
                            if (masjidListModel.getId().equalsIgnoreCase(homeMasjidPostList.getMasjidId())) {
                                mFilterList.add(homeMasjidPostList);
                            }
                        }

                        if (mFilterList.size() > 0) {

                            List<RecyclerViewItem> recyclerViewItems = new ArrayList<>();
                            for (HomeMasjidPostList masjidListModel : mFilterList){
                                recyclerViewItems.add(masjidListModel);
                            }

                            mAdapter = new HomeSelectedAdapter(activity, recyclerViewItems, masjidListModel);
                            mRecyclerView.setAdapter(mAdapter);
                        }

                    } else {
                        swipeRefreshLayout.setVisibility(View.GONE);
                        errorLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipeRefreshLayout.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });
    }
}

