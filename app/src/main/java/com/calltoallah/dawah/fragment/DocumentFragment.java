package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.MasjidListModel;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class DocumentFragment extends BaseFragment {

    private Bundle bundle;
    private MasjidListModel masjidListModel;
    private int mode;
    private EditText edtRegistrationNumber, edtBankName, edtAccountHolderName, edtAccountNumber,
            edtIFSCCode, edtAddress, edtAadharNumber, edtPanNumber;
    private ImageView imagePancard, imageSignature, imageAadharBack, imageAadharFront, imageVideo;
    private LinearLayout linearMasjidContainer, linearUserContainer,
            linearAadharFrontContainer, linearAadharBackContainer;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_document_info, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {

        bundle = getArguments();
        if (bundle != null) {
            mode = bundle.getInt(Constants.MODE);
            if (mode == 1) {
                masjidListModel = bundle.getParcelable(Constants.DATA);
            }
        }

        Init(v);
        setData();
    }

    private void setData() {
        if (mode == 0) {
            linearMasjidContainer.setVisibility(View.GONE);
            linearUserContainer.setVisibility(View.VISIBLE);
            linearAadharFrontContainer.setVisibility(View.VISIBLE);
            linearAadharBackContainer.setVisibility(View.VISIBLE);

            String address = "";
            if (UserPreferences.loadUser(getActivity()).getAddress().equalsIgnoreCase("") ||
                    UserPreferences.loadUser(getActivity()).getAddress() == null) {
                address = "Not Found";
            } else {
                address = UserPreferences.loadUser(getActivity()).getAddress();
            }

            edtAddress.setText(address);

            String aadharNumber = "";
            if (UserPreferences.loadUser(getActivity()).getUidai_number() == null) {
                aadharNumber = "Not Found";
            } else {
                aadharNumber = UserPreferences.loadUser(getActivity()).getUidai_number();
            }

            edtAadharNumber.setText(aadharNumber);

            String panNumber = "";
            if (UserPreferences.loadUser(getActivity()).getPan_card_number() == null) {
                panNumber = "Not Found";
            } else {
                panNumber = UserPreferences.loadUser(getActivity()).getPan_card_number();
            }

            edtPanNumber.setText(panNumber);

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.ic_action_camera)
                    .error(R.drawable.ic_action_camera);

            Glide.with(getActivity())
                    .load(API_DOMAIN + UserPreferences.loadUser(getActivity()).getPan_card_photo())
                    .apply(options)
                    .into(imagePancard);

            Glide.with(getActivity())
                    .load(API_DOMAIN + UserPreferences.loadUser(getActivity()).getFront_photo_id())
                    .apply(options)
                    .into(imageAadharFront);

            Glide.with(getActivity())
                    .load(API_DOMAIN + UserPreferences.loadUser(getActivity()).getBack_photo_id())
                    .apply(options)
                    .into(imageAadharBack);

            Glide.with(getActivity())
                    .load(API_DOMAIN + UserPreferences.loadUser(getActivity()).getSignature_photo())
                    .apply(options)
                    .into(imageSignature);

            imageVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserPreferences.loadUser(getActivity()).getShort_video() != null) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse(API_DOMAIN + UserPreferences.loadUser(getActivity()).getShort_video()), "video/*");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //DO NOT FORGET THIS EVER
                        startActivity(Intent.createChooser(intent, "Complete action using"));
                    }
                }
            });

        } else {
            linearMasjidContainer.setVisibility(View.VISIBLE);
            linearUserContainer.setVisibility(View.GONE);
            linearAadharFrontContainer.setVisibility(View.GONE);
            linearAadharBackContainer.setVisibility(View.GONE);

            String regNumber = "";
            if (masjidListModel.getRegistrationNumber().equalsIgnoreCase("") ||
                    masjidListModel.getRegistrationNumber() == null) {
                regNumber = "Not Found";
            } else {
                regNumber = masjidListModel.getRegistrationNumber();
            }

            edtRegistrationNumber.setText(regNumber);

            String bankName = "";
            if (masjidListModel.getBankName().equalsIgnoreCase("") ||
                    masjidListModel.getBankName() == null) {
                bankName = "Not Found";
            } else {
                bankName = masjidListModel.getBankName();
            }

            edtBankName.setText(bankName);

            String accountHolder = "";
            if (masjidListModel.getAccountHolderName().equalsIgnoreCase("") ||
                    masjidListModel.getAccountHolderName() == null) {
                accountHolder = "Not Found";
            } else {
                accountHolder = masjidListModel.getAccountHolderName();
            }

            edtAccountHolderName.setText(accountHolder);

            String accountNumber = "";
            if (masjidListModel.getAccountNumber().equalsIgnoreCase("") ||
                    masjidListModel.getAccountNumber() == null) {
                accountNumber = "Not Found";
            } else {
                accountNumber = masjidListModel.getAccountNumber();
            }

            edtAccountNumber.setText(accountNumber);

            String ifscCode = "";
            if (masjidListModel.getIfscCode().equalsIgnoreCase("") ||
                    masjidListModel.getIfscCode() == null) {
                ifscCode = "Not Found";
            } else {
                ifscCode = masjidListModel.getIfscCode();
            }

            edtIFSCCode.setText(ifscCode);

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.ic_action_camera)
                    .error(R.drawable.ic_action_camera);

            Glide.with(getActivity())
                    .load(API_DOMAIN + masjidListModel.getPanCardPhoto())
                    .apply(options)
                    .into(imagePancard);

            Glide.with(getActivity())
                    .load(API_DOMAIN + masjidListModel.getSignaturePhoto())
                    .apply(options)
                    .into(imageSignature);

            imageVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (masjidListModel.getShortVideo() != null) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse(API_DOMAIN + masjidListModel.getShortVideo()), "video/*");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //DO NOT FORGET THIS EVER
                        startActivity(Intent.createChooser(intent, "Complete action using"));
                    }
                }
            });
        }
    }

    private void Init(View v) {
        linearMasjidContainer = v.findViewById(R.id.linearMasjidContainer);
        linearUserContainer = v.findViewById(R.id.linearUserContainer);
        linearAadharFrontContainer = v.findViewById(R.id.linearAadharFrontContainer);
        linearAadharBackContainer = v.findViewById(R.id.linearAadharBackContainer);

        edtRegistrationNumber = (EditText) v.findViewById(R.id.edtRegistrationNumber);
        edtBankName = (EditText) v.findViewById(R.id.edtBankName);
        edtAccountHolderName = (EditText) v.findViewById(R.id.edtAccountHolderName);
        edtAccountNumber = (EditText) v.findViewById(R.id.edtAccountNumber);
        edtIFSCCode = (EditText) v.findViewById(R.id.edtIFSCCode);
        edtAddress = (EditText) v.findViewById(R.id.edtAddress);
        edtAadharNumber = (EditText) v.findViewById(R.id.edtAadharNumber);
        edtPanNumber = (EditText) v.findViewById(R.id.edtPanNumber);

        imagePancard = (ImageView) v.findViewById(R.id.imagePancard);
        imageSignature = (ImageView) v.findViewById(R.id.imageSignature);
        imageAadharFront = (ImageView) v.findViewById(R.id.imageAadharFront);
        imageAadharBack = (ImageView) v.findViewById(R.id.imageAadharBack);
        imageVideo = (ImageView) v.findViewById(R.id.imageVideo);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}