package com.calltoallah.dawah.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.pdf.FailureResponse;
import com.calltoallah.dawah.pdf.PdfGenerator;
import com.calltoallah.dawah.pdf.PdfGeneratorListener;
import com.calltoallah.dawah.pdf.SuccessResponse;
import com.calltoallah.dawah.qrcode_generator.QRGContents;
import com.calltoallah.dawah.qrcode_generator.QRGEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.content.Context.WINDOW_SERVICE;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class MasjidQRCodeFragment extends BaseFragment {

    private MasjidListModel masjidListModel;
    private TextView masjidFullname, masjidUsername, masjidAddress;
    private ImageView masjidPhoto, qrImage;
    private Bitmap bitmap;
    private QRGEncoder qrgEncoder;
    private Button btnShare, btnDownloadPDF;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_masjid_qrcode, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        if (getArguments() != null) {
            masjidListModel = getArguments().getParcelable(Constants.DATA);
        }

        Init(v);
        setData();
    }

    private void setData() {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.no_profile_image)
                .error(R.drawable.no_profile_image);

        Glide.with(getActivity())
                .load(API_DOMAIN + masjidListModel.getImagePath())
                .apply(options)
                .circleCrop()
                .into(masjidPhoto);

        if (masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
            masjidFullname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
            masjidFullname.setCompoundDrawablePadding(5);
        }

        masjidFullname.setText(masjidListModel.getMasjidName());
        masjidUsername.setText(masjidListModel.getMasjidUsername());
        masjidAddress.setText(masjidListModel.getAddress());

        // QR Code configure ...
        WindowManager manager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;

        qrgEncoder = new QRGEncoder(
                masjidListModel.getQr_code(), null,
                QRGContents.Type.TEXT,
                smallerDimension);
        qrgEncoder.setColorBlack(Color.parseColor("#3A5C4E"));
        qrgEncoder.setColorWhite(Color.parseColor("#FFFFFF"));
        try {
            bitmap = qrgEncoder.getBitmap();
            qrImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnDownloadPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View content = inflater.inflate(R.layout.fragment_masjid_qrcode, null);

                List<View> views = new ArrayList<>();
                views.add(content);

                PdfGenerator.getBuilder()
                        .setContext(getActivity())
                        .fromViewIDSource()
                        .fromViewID(getActivity(), R.id.qrContainer)
                        .setDefaultPageSize(PdfGenerator.PageSize.A4)
                        .setFileName("" + masjidListModel.getMasjidUsername() + "-" + masjidListModel.getId() + new Random().nextInt())
                        .setFolderName(getString(R.string.app_name))
                        .openPDFafterGeneration(true)
                        .build(new PdfGeneratorListener() {
                            @Override
                            public void onFailure(FailureResponse failureResponse) {
                                super.onFailure(failureResponse);
                                Toast.makeText(activity, "Unable to create pdf!", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void showLog(String log) {
                                super.showLog(log);
                            }

                            @Override
                            public void onSuccess(SuccessResponse response) {
                                super.onSuccess(response);
                                Toast.makeText(activity, "Successfully Created!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = masjidListModel.getMasjidName() + "\n\n" +
                        "Get this masjid/madrasah from below link:" + "\n" +
                        getActivity().getString(R.string.panel_url) + masjidListModel.getId();

                String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, getString(R.string.app_name), null);
                Uri uri = Uri.parse(path);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("image/jpeg");
                intent.setPackage("com.whatsapp");
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                intent.putExtra(Intent.EXTRA_SUBJECT, getActivity().getString(R.string.app_name));
                intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(intent, "Share Image"));
            }
        });
    }

    private void Init(View v) {
        btnShare = (Button) v.findViewById(R.id.btnShare);
        btnDownloadPDF = (Button) v.findViewById(R.id.btnDownloadPDF);
        qrImage = (ImageView) v.findViewById(R.id.qrImage);

        masjidPhoto = (ImageView) v.findViewById(R.id.masjidPhoto);
        masjidFullname = (TextView) v.findViewById(R.id.masjidFullname);
        masjidUsername = (TextView) v.findViewById(R.id.masjidUsername);
        masjidAddress = (TextView) v.findViewById(R.id.masjidAddress);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);

//        Bundle bundle = new Bundle();
//        bundle.putParcelable(Constants.DATA, masjidListModel);
//
//        MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
//        masjidDetailsFragment.setArguments(bundle);
//        activity.loadFragment(masjidDetailsFragment);
    }
}