package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.StringAdapter;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.CustomerModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

public class CustomerSupportFragment extends BaseFragment {

    private TextView txtCategory, txtSubCategory;
    private LinearLayout linearSubCategory;
    private List<String> keyValue = new ArrayList<>();
    private Button btnQuerySubmit;
    private EditText edtComments;
    private TextView txtUsername;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_customer_support, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {
        txtUsername.setText("Hello, " + UserPreferences.loadUser(getActivity()).getFullName());

        List<String> list1 = new ArrayList<String>();
        list1.add("Profile Update");
        list1.add("Verification Request");
        list1.add("Need to Change My Personal Details");
        list1.add("Status of My Donation");
        list1.add("Money Deducted but Not Paid to Masajids");
        CustomerModel customerModel = new CustomerModel(1, "Payment Related", list1);

        List<String> list2 = new ArrayList<String>();
        list2.add("Unable to Add a Masajids and Madarasahs");
        list2.add("Pending Listing of Masajids and Madarasahs");
        list2.add("Pending Verification of Masajids and Madarasahs");
        list2.add("Need to Change Bank Details");
        list2.add("Problem in Updating Time Tables");
        CustomerModel customerModel1 = new CustomerModel(2, "Masajids and Madarasahs", list2);

        List<String> list3 = new ArrayList<String>();
        list3.add("Report a Masajids and Madarasahs");
        list3.add("Report a User");
        CustomerModel customerModel2 = new CustomerModel(3, "Report", list3);

        List<String> list4 = new ArrayList<String>();
        list4.add("Donate in Project");
        list4.add("Get Partnership");
        list4.add("Become Volunteer");
        CustomerModel customerModel3 = new CustomerModel(4, "Suggestions", list4);

        List<CustomerModel> mCustomerQuery = new ArrayList<>();
        mCustomerQuery.add(customerModel);
        mCustomerQuery.add(customerModel1);
        mCustomerQuery.add(customerModel2);
        mCustomerQuery.add(customerModel3);

        txtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<String> keyName = new ArrayList<>();
                for (CustomerModel category : mCustomerQuery) {
                    keyName.add(category.getName());
                }

                View parentView = getLayoutInflater().inflate(R.layout.dialog_list, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                dialog.setContentView(parentView);
                dialog.show();

                TextView menuText = (TextView) dialog.findViewById(R.id.menuText);
                menuText.setText("Your Query: ");

                RecyclerView menuList = (RecyclerView) dialog.findViewById(R.id.menuList);
                LinearLayoutManager lm = new LinearLayoutManager(getActivity());
                menuList.setLayoutManager(lm);
                menuList.setHasFixedSize(true);

                StringAdapter stringAdapter = new StringAdapter(getActivity(), keyName);
                menuList.setAdapter(stringAdapter);
                stringAdapter.notifyDataSetChanged();

                stringAdapter.setOnItemClickListener(new StringAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        dialog.dismiss();
                        keyValue = mCustomerQuery.get(position).getSubCategoryList();
                        linearSubCategory.setVisibility(View.VISIBLE);
                        txtCategory.setText(keyName.get(position));
                    }
                });
            }
        });

        txtSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View parentView = getLayoutInflater().inflate(R.layout.dialog_list, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                dialog.setContentView(parentView);
                dialog.show();

                TextView menuText = (TextView) dialog.findViewById(R.id.menuText);
                menuText.setText("Please tell us more: ");

                RecyclerView menuList = (RecyclerView) dialog.findViewById(R.id.menuList);
                LinearLayoutManager lm = new LinearLayoutManager(getActivity());
                menuList.setLayoutManager(lm);
                menuList.setHasFixedSize(true);

                StringAdapter stringAdapter = new StringAdapter(getActivity(), keyValue);
                menuList.setAdapter(stringAdapter);
                stringAdapter.notifyDataSetChanged();

                stringAdapter.setOnItemClickListener(new StringAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        dialog.dismiss();
                        txtSubCategory.setText(keyValue.get(position));
                    }
                });
            }
        });

        btnQuerySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCategory.getText().toString().equalsIgnoreCase("Your Query: ")) {
                    Toast.makeText(getActivity(), "Please select your query", Toast.LENGTH_SHORT).show();
                } else if (txtCategory.getText().toString().equalsIgnoreCase("Please tell us more: ")) {
                    Toast.makeText(getActivity(), "Please select your sub query", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                        String version = pInfo.versionName;

                        String categoryName = txtCategory.getText().toString();
                        String subCategoryName = txtSubCategory.getText().toString();
                        String comments = edtComments.getText().toString();
                        String customBody = categoryName + "\n" + subCategoryName + "\n\n" + comments + "\n" + "Device version: " + version;

                        Intent intent = new Intent(Intent.ACTION_SEND);
                        String[] recipients = {"dawah@calltoallah.com"};
                        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");
                        intent.putExtra(Intent.EXTRA_TEXT, customBody);
                        intent.setType("text/html");
                        intent.setPackage("com.google.android.gm");
                        startActivity(Intent.createChooser(intent, "Send mail"));

                        activity.startActivity(new Intent(getActivity(), MainActivity.class));

                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void Init(View v) {
        linearSubCategory = (LinearLayout) v.findViewById(R.id.linearSubCategory);
        btnQuerySubmit = (Button) v.findViewById(R.id.btnQuerySubmit);
        edtComments = (EditText) v.findViewById(R.id.edtComments);
        txtCategory = v.findViewById(R.id.txtCategory);
        txtSubCategory = v.findViewById(R.id.txtSubCategory);
        txtUsername = v.findViewById(R.id.txtUsername);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}
