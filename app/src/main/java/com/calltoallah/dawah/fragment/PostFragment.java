package com.calltoallah.dawah.fragment;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.calltoallah.dawah.DonationActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.HomeMasjidPostList;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.model.PostModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.android.exoplayer2.BuildConfig;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class PostFragment extends BaseFragment {

    private PostModel mediaObject;
    private HomeMasjidPostList homeMasjidPostList;
    private LinearLayout donationContainer, masjidContainer;
    public ProgressBar progressDonation;
    private TextView txtPostInfo, txtMasjidName, txtCreatedDate, txtDonate, txtDonationAmt, txtLikeCount, txtShareCount;
    private ImageView postLike, postShare, imgMasjidIcon;
    private int mode;

    public ImageView thumbnail;
    private FrameLayout media_container;
    private ProgressBar mLoading;
    private SimpleExoPlayer player;
    private PlayerView playerView;
    private DefaultTrackSelector trackSelector;
    private DataSource.Factory dataSourceFactory;
    private FrameworkMediaDrm mediaDrm;
    private ImageView exo_reply, exo_rewind, exo_forward;
    private RelativeLayout exoContainer;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_post_details, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        if (getArguments() != null) {
            mode = getArguments().getInt(Constants.MODE);

            if (mode == 15) {
                homeMasjidPostList = getArguments().getParcelable(Constants.DATA);
            } else {
                mediaObject = getArguments().getParcelable(Constants.DATA);
            }
        }

        Init(v);

        configureDataFactory();
        configurePlayer();
        setData();
    }

    private void configureDataFactory() {
        dataSourceFactory =
                new DefaultDataSourceFactory(
                        getActivity(), Util.getUserAgent(getActivity(), getString(R.string.app_name)));
    }

    public boolean useExtensionRenderers() {
        return "withExtensions".equals(BuildConfig.FLAVOR);
    }

    private void configurePlayer() {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        final TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        trackSelector.setParameters(new DefaultTrackSelector.ParametersBuilder().
                setRendererDisabled(C.TRACK_TYPE_VIDEO, false).build());

        boolean preferExtensionDecoders = false;
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionDecoders ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        DefaultRenderersFactory renderersFactory = new DefaultRenderersFactory(getActivity(), extensionRendererMode);

        player = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector);
        playerView.setPlayer(player);
        player.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if (playbackState == ExoPlayer.STATE_READY) {
                    exo_rewind.setVisibility(VISIBLE);
                    exo_forward.setVisibility(VISIBLE);
                    exo_reply.setImageResource(R.drawable.ic_action_play);
                    mLoading.setVisibility(View.INVISIBLE);

                } else if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    exo_rewind.setVisibility(VISIBLE);
                    exo_forward.setVisibility(VISIBLE);
                    exo_reply.setImageResource(R.drawable.ic_action_play);
                    mLoading.setVisibility(View.VISIBLE);

                } else if (playbackState == ExoPlayer.STATE_ENDED) {
                    exo_rewind.setVisibility(GONE);
                    exo_forward.setVisibility(GONE);
                    exo_reply.setImageResource(R.drawable.ic_action_replay);

                } else if (playbackState == ExoPlayer.STATE_IDLE) {
                    mLoading.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                switch (error.type) {
                    case ExoPlaybackException.TYPE_SOURCE:
                        Log.e("Error", "TYPE_SOURCE: " + error.getSourceException().getMessage());
                        break;

                    case ExoPlaybackException.TYPE_RENDERER:
                        Log.e("Error", "TYPE_RENDERER: " + error.getRendererException().getMessage());
                        break;

                    case ExoPlaybackException.TYPE_UNEXPECTED:
                        Log.e("Error", "TYPE_UNEXPECTED: " + error.getUnexpectedException().getMessage());
                        break;
                }

                if (error instanceof ExoPlaybackException
                        && error.getCause() instanceof BehindLiveWindowException) {
                    isBehindLiveWindow(error);
                }
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    private void setData() {

        if (mode == 15) {
            if (homeMasjidPostList.getDocPath().equalsIgnoreCase("")) {
                media_container.setVisibility(View.GONE);
                txtPostInfo.setBackgroundResource(R.color.colorPrimary);
                txtPostInfo.setTextColor(Color.WHITE);
                txtPostInfo.setGravity(Gravity.CENTER);

                ViewGroup.LayoutParams params = txtPostInfo.getLayoutParams();
                params.height = 400;
                txtPostInfo.setLayoutParams(params);

            } else {
                media_container.setVisibility(View.VISIBLE);
                txtPostInfo.setBackgroundResource(R.color.colorTransparent);
                txtPostInfo.setTextColor(Color.BLACK);
                txtPostInfo.setGravity(Gravity.LEFT);

                ViewGroup.LayoutParams params = txtPostInfo.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                txtPostInfo.setLayoutParams(params);

                String urlExtension = homeMasjidPostList.getDocPath().substring(homeMasjidPostList.getDocPath().lastIndexOf("."));
                if (urlExtension.equalsIgnoreCase(".mp4")) {
                    exoContainer.setVisibility(VISIBLE);

                    // MP4 Streaming ...
                    MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(API_DOMAIN + homeMasjidPostList.getDocPath()));
                    player.prepare(videoSource);
                    setPlayPause(true);

                } else if (urlExtension.equalsIgnoreCase(".m3u8")) {
                    exoContainer.setVisibility(VISIBLE);

                    // HLS Streaming ...
                    HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(API_DOMAIN + homeMasjidPostList.getDocPath()));
                    player.prepare(videoSource);
                    setPlayPause(true);

                } else {
                    exoContainer.setVisibility(GONE);

                    Glide.with(getActivity())
                            .load(API_DOMAIN + homeMasjidPostList.getDocPath())
                            .into(thumbnail);
                }
            }

            // For Danation ...
            if (homeMasjidPostList.getDonationButton().equalsIgnoreCase("1")) {
                donationContainer.setVisibility(View.VISIBLE);

                int userDonated = Integer.parseInt(homeMasjidPostList.getUser_donated());
                int userDonatedAmt = Integer.parseInt(homeMasjidPostList.getAsked_donation_amount());

                progressDonation.setMax(userDonatedAmt);
                progressDonation.setProgress(userDonated);
                txtDonationAmt.setText("₹ " + homeMasjidPostList.getUser_donated() + " Collected from the target of ₹ " +
                        homeMasjidPostList.getAsked_donation_amount());

                if (userDonated >= userDonatedAmt) {
                    txtDonate.setText("Donated");
                    txtDonate.setBackgroundTintList(ColorStateList.valueOf(getActivity().getResources().getColor(R.color.colorGray)));
                    txtDonate.setEnabled(false);
                } else {
                    txtDonate.setEnabled(true);
                }

            } else {
                donationContainer.setVisibility(View.GONE);
            }

            txtLikeCount.setText(prettyCount(Integer.parseInt(homeMasjidPostList.getLike_count())));
            txtShareCount.setText(prettyCount(Integer.parseInt(homeMasjidPostList.getShare_count())));

            txtMasjidName.setText(homeMasjidPostList.getMasjidName());
            txtPostInfo.setText(homeMasjidPostList.getPostText());

            if (homeMasjidPostList.getVerified_status().equalsIgnoreCase("1")) {
                txtMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                txtMasjidName.setCompoundDrawablePadding(5);
            }

            if (homeMasjidPostList.getCreatedOn() != null) {
                String inputPattern = "yyyy-MM-dd HH:mm:ss";
                String outputPattern = "EEEE, dd MMMM, yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                try {
                    Date date = inputFormat.parse(homeMasjidPostList.getCreatedOn());
                    String createdDate = outputFormat.format(date);
                    txtCreatedDate.setText(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            Glide.with(activity)
                    .load(API_DOMAIN + homeMasjidPostList.getMasjidImage())
                    .into(imgMasjidIcon);

            masjidContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMasjidDetails(homeMasjidPostList.getMasjidId());
                }
            });

            if (homeMasjidPostList.isPost_liked() == true) {
                postLike.setImageResource(R.drawable.ic_action_like);
            } else {
                postLike.setImageResource(R.drawable.ic_action_unlike);
            }

            postLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.getPostsLikeUnlike(homeMasjidPostList.getMasjidId(),
                            homeMasjidPostList.getId(), UserPreferences.loadUser(activity).getId());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.d("POST_LIKE: ", response.body().toString());

                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    if (jsonObject.getString("message").equalsIgnoreCase("Post liked")) {
                                        int likeCount = Integer.parseInt(homeMasjidPostList.getLike_count()) + 1;
                                        homeMasjidPostList.setLike_count(String.valueOf(likeCount));
                                        homeMasjidPostList.setPost_liked(true);

                                        postLike.setImageResource(R.drawable.ic_action_like);
                                        txtLikeCount.setText(prettyCount(likeCount));

                                    } else {
                                        int likeCount = Integer.parseInt(homeMasjidPostList.getLike_count()) - 1;
                                        homeMasjidPostList.setLike_count(String.valueOf(likeCount));
                                        homeMasjidPostList.setPost_liked(false);

                                        postLike.setImageResource(R.drawable.ic_action_unlike);
                                        txtLikeCount.setText(prettyCount(likeCount));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });
                }
            });

            postShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.getPostsShare(
                            homeMasjidPostList.getMasjidId(), homeMasjidPostList.getId(),
                            UserPreferences.loadUser(activity).getId());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            Log.d("POST_SHARE: ", response.body().toString());

                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    String message = jsonObject.getString("message");


                                    String shareBody = homeMasjidPostList.getMasjidName() + "\n\n" +
                                            "Get this masjid/madrasah from below link:" + "\n" +
                                            activity.getString(R.string.panel_url) + homeMasjidPostList.getMasjidId() + ".html";
                                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                    sharingIntent.setType("text/plain");
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.app_name));
                                    activity.startActivity(Intent.createChooser(sharingIntent, activity.getResources().getString(R.string.app_name)));

                                    int shareCount = Integer.parseInt(homeMasjidPostList.getShare_count()) + 1;
                                    homeMasjidPostList.setShare_count(String.valueOf(shareCount));
                                    txtShareCount.setText(prettyCount(shareCount));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });
                }
            });

            txtDonate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1")) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.MODE, 0);
                        bundle.putParcelable(Constants.DATA, homeMasjidPostList);

                        Intent intent = new Intent(getActivity(), DonationActivity.class);
                        intent.putExtras(bundle);
                        activity.startActivity(intent);

                    } else {
                        activity.loadFragment(new UserVerificationFragment());
                    }
                }
            });

        } else {
            if (mediaObject.getDocPath().equalsIgnoreCase("")) {
                media_container.setVisibility(View.GONE);
                txtPostInfo.setBackgroundResource(R.color.colorPrimary);
                txtPostInfo.setTextColor(Color.WHITE);
                txtPostInfo.setGravity(Gravity.CENTER);

                ViewGroup.LayoutParams params = txtPostInfo.getLayoutParams();
                params.height = 400;
                txtPostInfo.setLayoutParams(params);

            } else {
                media_container.setVisibility(View.VISIBLE);
                txtPostInfo.setBackgroundResource(R.color.colorTransparent);
                txtPostInfo.setTextColor(Color.BLACK);
                txtPostInfo.setGravity(Gravity.LEFT);

                ViewGroup.LayoutParams params = txtPostInfo.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                txtPostInfo.setLayoutParams(params);

                String urlExtension = mediaObject.getDocPath().substring(mediaObject.getDocPath().lastIndexOf("."));
                if (urlExtension.equalsIgnoreCase(".mp4")) {
                    exoContainer.setVisibility(VISIBLE);

                    // MP4 Streaming ...
                    MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(API_DOMAIN + mediaObject.getDocPath()));
                    player.prepare(videoSource);
                    setPlayPause(true);

                } else if (urlExtension.equalsIgnoreCase(".m3u8")) {
                    exoContainer.setVisibility(VISIBLE);

                    // HLS Streaming ...
                    HlsMediaSource videoSource = new HlsMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(API_DOMAIN + mediaObject.getDocPath()));
                    player.prepare(videoSource);
                    setPlayPause(true);

                } else {
                    exoContainer.setVisibility(GONE);

                    Glide.with(getActivity())
                            .load(API_DOMAIN + mediaObject.getDocPath())
                            .into(thumbnail);
                }
            }

            // For Danation ...
            if (mediaObject.getDonationButton().equalsIgnoreCase("1")) {
                donationContainer.setVisibility(View.VISIBLE);

                int userDonated = Integer.parseInt(mediaObject.getUserDonated());
                int userDonatedAmt = Integer.parseInt(mediaObject.getAskedDonationAmount());

                progressDonation.setMax(userDonatedAmt);
                progressDonation.setProgress(userDonated);
                txtDonationAmt.setText("₹ " + mediaObject.getUserDonated() + " Collected from the target of ₹ " + mediaObject.getAskedDonationAmount());

            } else {
                donationContainer.setVisibility(View.GONE);
            }

            if (UserPreferences.loadUser(getActivity()).getVerify_status().equalsIgnoreCase("1")) {
                txtDonate.setEnabled(true);
            } else {
                txtDonate.setEnabled(false);
            }

            txtLikeCount.setText(prettyCount(Integer.parseInt(mediaObject.getLikeCount())));
            txtShareCount.setText(prettyCount(Integer.parseInt(mediaObject.getShareCount())));

            txtMasjidName.setText(mediaObject.getMasjidName());
            txtPostInfo.setText(mediaObject.getPostText());

            if (mediaObject.getVerifiedStatus().equalsIgnoreCase("1")) {
                txtMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                txtMasjidName.setCompoundDrawablePadding(5);
            }

            if (mediaObject.getCreatedOn() != null) {
                String inputPattern = "yyyy-MM-dd HH:mm:ss";
                String outputPattern = "EEEE, dd MMMM, yyyy";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                try {
                    Date date = inputFormat.parse(mediaObject.getCreatedOn());
                    String createdDate = outputFormat.format(date);
                    txtCreatedDate.setText(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            Glide.with(activity)
                    .load(API_DOMAIN + mediaObject.getMasjidImage())
                    .into(imgMasjidIcon);

            masjidContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMasjidDetails(mediaObject.getMasjidId());
                }
            });

            if (mediaObject.isPost_liked() == true) {
                postLike.setImageResource(R.drawable.ic_action_like);
            } else {
                postLike.setImageResource(R.drawable.ic_action_unlike);
            }

            postLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.getPostsLikeUnlike(mediaObject.getMasjidId(),
                            mediaObject.getId(), UserPreferences.loadUser(activity).getId());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    if (jsonObject.getString("message").equalsIgnoreCase("Post liked")) {
                                        int likeCount = Integer.parseInt(mediaObject.getLikeCount()) + 1;
                                        mediaObject.setLikeCount(String.valueOf(likeCount));
                                        mediaObject.setPost_liked(true);

                                    } else {
                                        int likeCount = Integer.parseInt(mediaObject.getLikeCount()) - 1;
                                        mediaObject.setLikeCount(String.valueOf(likeCount));
                                        mediaObject.setPost_liked(false);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });
                }
            });

            postShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                    final Call<String> loginCall = apiInterface.getPostsShare(
                            mediaObject.getMasjidId(), mediaObject.getId(),
                            UserPreferences.loadUser(activity).getId());
                    loginCall.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {
                            if (response.isSuccessful()) {
                                Log.d("SHARE: ", response.body().toString());

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().toString());
                                    String message = jsonObject.getString("message");


                                    String shareBody = mediaObject.getMasjidName() + "\n\n" +
                                            "Get this masjid/madrasah from below link:" + "\n" +
                                            activity.getString(R.string.panel_url) + mediaObject.getMasjidId() + ".html";
                                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                    sharingIntent.setType("text/plain");
                                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.app_name));
                                    activity.startActivity(Intent.createChooser(sharingIntent, activity.getResources().getString(R.string.app_name)));

                                    int shareCount = Integer.parseInt(mediaObject.getShareCount()) + 1;
                                    mediaObject.setShareCount(String.valueOf(shareCount));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                        }
                    });
                }
            });

            txtDonate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.MODE, 0);
                    bundle.putParcelable(Constants.DATA, mediaObject);

                    Intent intent = new Intent(getActivity(), DonationActivity.class);
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                }
            });
        }
    }

    private void setPlayPause(boolean play) {
        player.setPlayWhenReady(play);
    }

    public String prettyCount(long numValue) {
        char[] suffix = {' ', 'K', 'M', 'B', 'T', 'P', 'E'};
        int value = (int) Math.floor(Math.log10(numValue));
        int base = value / 3;
        if (value >= 3 && base < suffix.length) {
            return new DecimalFormat("#0.0").format(numValue / Math.pow(10, base * 3)) + suffix[base];
        } else {
            return new DecimalFormat("#,##0").format(numValue);
        }
    }

    private void getMasjidDetails(String masjidID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getMasjidDetails(masjidID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                        if (masjidModel.getMasjidListModel().size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.MODE, 0);
                            bundle.putParcelable(Constants.DATA, masjidModel.getMasjidListModel().get(0));

                            MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                            masjidDetailsFragment.setArguments(bundle);
                            activity.loadFragment(masjidDetailsFragment);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }

    private void Init(View v) {
        media_container = v.findViewById(R.id.media_container);
        thumbnail = v.findViewById(R.id.thumbnail);

        txtPostInfo = v.findViewById(R.id.txtPostInfo);
        txtMasjidName = v.findViewById(R.id.txtMasjidName);
        txtDonate = v.findViewById(R.id.txtDonate);
        txtCreatedDate = v.findViewById(R.id.txtCreatedDate);
        postLike = v.findViewById(R.id.postLike);
        postShare = v.findViewById(R.id.postShare);
        imgMasjidIcon = v.findViewById(R.id.imgMasjidIcon);
        txtDonationAmt = v.findViewById(R.id.txtDonationAmt);
        donationContainer = v.findViewById(R.id.donationContainer);
        progressDonation = (ProgressBar) v.findViewById(R.id.progressDonation);
        masjidContainer = v.findViewById(R.id.masjidContainer);
        txtLikeCount = (TextView) v.findViewById(R.id.txtLikeCount);
        txtShareCount = (TextView) v.findViewById(R.id.txtShareCount);

        txtDonationAmt.setSelected(true);

        playerView = (PlayerView) v.findViewById(R.id.sepv);
        mLoading = (ProgressBar) v.findViewById(R.id.mLoading);
        exoContainer = (RelativeLayout) v.findViewById(R.id.exoContainer);

        exo_reply = (ImageView) v.findViewById(R.id.exo_play);
        exo_rewind = (ImageView) v.findViewById(R.id.exo_rew);
        exo_forward = (ImageView) v.findViewById(R.id.exo_ffwd);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);

        if (player != null) {
            releasePlayer();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (player != null) {
            releasePlayer();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (player != null) {
            releasePlayer();
        }
    }

    public void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }
}