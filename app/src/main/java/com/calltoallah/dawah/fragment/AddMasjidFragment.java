package com.calltoallah.dawah.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.BuildConfig;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.MapActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.adapter.TimeTableAdapter;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.ProgressbarManager;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.location.SimplePlacePicker;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.TimeTableModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static com.calltoallah.dawah.MainActivity.mToolbarTitle;
import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class AddMasjidFragment extends BaseFragment {

    private EditText edtMasjidName, edtAddress, edtContactNumber, edtMasjidIcon, edtDescription, edtUsername, edtTimeTable;
    private Button btnAddMasjid;
    private ProgressbarManager progressbarManager;
    private int PLACE_PICKER_REQUEST = 1;
    private String imagePath;
    private double masjidLat = 0, masjidLon = 0;

    private static final String IMAGE_DIRECTORY_NAME = "Dawah";
    private static final int REQUEST_TAKE_PHOTO = 02;
    private static final int REQUEST_PICK_PHOTO = 13;
    private static final int CAMERA_PIC_REQUEST = 16;
    private Uri fileUri;
    private String mImageFileLocation = "";
    private boolean responseFailed = false;
    private String prayer_name, prayer_time;
    private List<TimeTableModel> timeTableModelList = new ArrayList<>();

    private Bundle bundle;
    private MasjidListModel masjidListModel;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_add_masjid, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        bundle = getArguments();
        if (bundle != null) {
            mToolbarTitle.setText("Update Masjid");
            masjidListModel = bundle.getParcelable(Constants.DATA);

            edtMasjidName.setText(masjidListModel.getMasjidName());
            edtAddress.setText(masjidListModel.getAddress());
            edtContactNumber.setText(masjidListModel.getPhoneNumber());
            edtDescription.setText(masjidListModel.getAboutMasjid());
            edtUsername.setText(masjidListModel.getMasjidUsername());
            edtTimeTable.setText(masjidListModel.getPrayerTime());

            masjidLat = Double.parseDouble(masjidListModel.getMasjidLat());
            masjidLon = Double.parseDouble(masjidListModel.getMasjidLng());
            edtMasjidIcon.setText(API_DOMAIN + masjidListModel.getImagePath());

        } else {
            mToolbarTitle.setText("Add Masjid");
        }

        edtMasjidIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose your photo");
                String[] images = {"Take From Gallery", "Take From Camera"};
                builder.setItems(images, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(galleryIntent, REQUEST_PICK_PHOTO);
                                break;

                            case 1:
                                captureImage();
                                break;
                        }
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        edtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                PingPlacePicker.IntentBuilder builder = new PingPlacePicker.IntentBuilder();
//                builder.setAndroidApiKey(getString(R.string.google_brows_api_key))
//                        .setMapsApiKey(getString(R.string.google_maps_key));
//                try {
//                    Intent placeIntent = builder.build(getActivity());
//                    startActivityForResult(placeIntent, PLACE_PICKER_REQUEST);
//                } catch (Exception ex) {
//                    // Google Play services is not available...
//                }

                String apiKey = getString(R.string.google_maps_key);
                String mCountry = "ind";
                String mLanguage = "en";
                String[] mSupportedAreas = "".split(",");
                startMapActivity(apiKey, mCountry, mLanguage, mSupportedAreas);
            }
        });

        btnAddMasjid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtMasjidName.getText().toString().equalsIgnoreCase("") ||
                        edtAddress.getText().toString().equalsIgnoreCase("") ||
                        edtContactNumber.getText().toString().equalsIgnoreCase("") ||
                        edtMasjidIcon.getText().toString().equalsIgnoreCase("") ||
                        edtDescription.getText().toString().equalsIgnoreCase("") ||
                        edtUsername.getText().toString().equalsIgnoreCase("") ||
                        edtTimeTable.getText().toString().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "All Fields Required!", Toast.LENGTH_SHORT).show();
                } else {
                    progressbarManager.show();

                    if (masjidListModel != null) {
                        updateMasjid();
                    } else {
                        addMasjid();
                    }
                }
            }
        });

        edtTimeTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_timetable);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlackCC)));
                dialog.show();

                Button btnProceed = (Button) dialog.findViewById(R.id.btnProceed);
                RecyclerView mTimeTable = (RecyclerView) dialog.findViewById(R.id.rcvTimeTable);
                mTimeTable.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                mTimeTable.setItemAnimator(new DefaultItemAnimator());

                timeTableModelList = new ArrayList<>();
                TimeTableModel timeTableModel1 = new TimeTableModel("Fajar", "05:30 AM");
                TimeTableModel timeTableModel2 = new TimeTableModel("Zuhar", "01:30 PM");
                TimeTableModel timeTableModel3 = new TimeTableModel("Asar", "05:10 PM");
                TimeTableModel timeTableModel4 = new TimeTableModel("Maghrib", "07:05 PM");
                TimeTableModel timeTableModel5 = new TimeTableModel("Isha", "08:45 PM");

                TimeTableModel timeTableModel6 = new TimeTableModel("Khutbatul Juma", "Optional");
                TimeTableModel timeTableModel7 = new TimeTableModel("Tulu", "Optional");
                TimeTableModel timeTableModel8 = new TimeTableModel("Guroob", "Optional");
                TimeTableModel timeTableModel9 = new TimeTableModel("Zawal", "Optional");
                TimeTableModel timeTableModel10 = new TimeTableModel("Eid Ul Fitr", "Optional");
                TimeTableModel timeTableModel11 = new TimeTableModel("Eid Uz Zuha", "Optional");

                timeTableModelList.add(timeTableModel1);
                timeTableModelList.add(timeTableModel2);
                timeTableModelList.add(timeTableModel3);
                timeTableModelList.add(timeTableModel4);
                timeTableModelList.add(timeTableModel5);
                timeTableModelList.add(timeTableModel6);
                timeTableModelList.add(timeTableModel7);
                timeTableModelList.add(timeTableModel8);
                timeTableModelList.add(timeTableModel9);
                timeTableModelList.add(timeTableModel10);
                timeTableModelList.add(timeTableModel11);

                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        List<String> mLabel = new ArrayList<>();
                        List<String> mTime = new ArrayList<>();

                        for (TimeTableModel timeTableModel : timeTableModelList) {
                            mLabel.add(timeTableModel.getTt_name());
                            mTime.add(timeTableModel.getTt_time());
                        }

                        prayer_name = mLabel.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
                        prayer_time = mTime.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
                        Log.d("prayer_time: ", prayer_time);
                        Log.d("prayer_name: ", prayer_name);

                        edtTimeTable.setText(prayer_time);
                    }
                });

                TimeTableAdapter tasbihReportAdapter = new TimeTableAdapter(activity, timeTableModelList);
                mTimeTable.setAdapter(tasbihReportAdapter);

                btnProceed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        List<String> mLabel = new ArrayList<>();
                        List<String> mTime = new ArrayList<>();

                        for (TimeTableModel timeTableModel : timeTableModelList) {
                            mLabel.add(timeTableModel.getTt_name());
                            mTime.add(timeTableModel.getTt_time());
                        }

                        prayer_name = mLabel.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
                        prayer_time = mTime.toString().replace(", ", ", ").replaceAll("[\\[.\\]]", "");
                        Log.d("prayer_time: ", prayer_time);
                        Log.d("prayer_name: ", prayer_name);

                        edtTimeTable.setText(prayer_time);
                    }
                });
            }
        });
    }

    private void addMasjid() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDate = df.format(c.getTime());

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        RequestBody user_id = RequestBody.create(MediaType.parse("multipart/form-data"), UserPreferences.loadUser(getActivity()).getId());
        RequestBody masjid_username = RequestBody.create(MediaType.parse("multipart/form-data"), edtUsername.getText().toString());
        RequestBody masjid_name = RequestBody.create(MediaType.parse("multipart/form-data"), edtMasjidName.getText().toString());
        RequestBody phone_number = RequestBody.create(MediaType.parse("multipart/form-data"), edtContactNumber.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), edtAddress.getText().toString());
        RequestBody created_on = RequestBody.create(MediaType.parse("multipart/form-data"), currentDate);
        RequestBody about_masjid = RequestBody.create(MediaType.parse("multipart/form-data"), edtDescription.getText().toString());
        RequestBody masjid_latitude = RequestBody.create(MediaType.parse("multipart/form-data"), masjidLat + "");
        RequestBody masjid_longitude = RequestBody.create(MediaType.parse("multipart/form-data"), masjidLon + "");
        RequestBody namaz_time = RequestBody.create(MediaType.parse("multipart/form-data"), prayer_time);
        RequestBody namaz_name = RequestBody.create(MediaType.parse("multipart/form-data"), prayer_name);
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), "0");

        File file = new File(imagePath);
        RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part image_path = MultipartBody.Part.createFormData("image_path", file.getName(), mFile);

        callAPI = apiInterface.addMasjidMadrasah(
                user_id, masjid_username, masjid_name, phone_number, address,
                created_on, about_masjid, masjid_latitude, masjid_longitude,
                namaz_time, namaz_name, type, image_path, null, null);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("RESPONSE: ", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            showDialog();
                        } else {
                            Toast.makeText(activity, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException |
                            JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private void updateMasjid() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<String> callAPI;

        RequestBody namaz_time = null, namaz_name = null;
        if (prayer_time != null && prayer_name != null) {
            namaz_time = RequestBody.create(MediaType.parse("multipart/form-data"), prayer_time);
            namaz_name = RequestBody.create(MediaType.parse("multipart/form-data"), prayer_name);
        }

        MultipartBody.Part file_name = null;
        if (imagePath != null) {
            File file = new File(imagePath);
            RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            file_name = MultipartBody.Part.createFormData("file_name", file.getName(), mFile);
        }

        RequestBody masjid_id = RequestBody.create(MediaType.parse("multipart/form-data"), masjidListModel.getId());
        RequestBody masjid_username = RequestBody.create(MediaType.parse("multipart/form-data"), edtUsername.getText().toString());
        RequestBody masjid_name = RequestBody.create(MediaType.parse("multipart/form-data"), edtMasjidName.getText().toString());
        RequestBody phone_number = RequestBody.create(MediaType.parse("multipart/form-data"), edtContactNumber.getText().toString());
        RequestBody address = RequestBody.create(MediaType.parse("multipart/form-data"), edtAddress.getText().toString());
        RequestBody about_masjid = RequestBody.create(MediaType.parse("multipart/form-data"), edtDescription.getText().toString());
        RequestBody masjid_latitude = RequestBody.create(MediaType.parse("multipart/form-data"), masjidLat + "");
        RequestBody masjid_longitude = RequestBody.create(MediaType.parse("multipart/form-data"), masjidLon + "");

        callAPI = apiInterface.updateMasjidMadrasah(masjid_id,
                masjid_name, masjid_username, address, phone_number,
                about_masjid, masjid_latitude, masjid_longitude, namaz_time, namaz_name, null, null, file_name);
        callAPI.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                progressbarManager.dismiss();

                try {
                    String res = response.body().toString();
                    Log.d("UPDATE_MASJID: ", res);

                    try {

                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {
                            showDialog();
                        } else {
                            Toast.makeText(activity, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (IllegalStateException |
                            JsonSyntaxException exception) {
                        responseFailed = true;
                    }

                    if (responseFailed) {
                        progressbarManager.dismiss();
                        Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                progressbarManager.dismiss();
            }
        });
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity(), R.style.DialogTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorBlackCC)));
        dialog.show();

        TextView txtUsername = dialog.findViewById(R.id.txtUsername);
        Button btnMail = dialog.findViewById(R.id.btnMail);
        Button btnOK = dialog.findViewById(R.id.btnOK);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        txtUsername.setText("Hey " + UserPreferences.loadUser(getActivity()).getFullName());
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });

        btnMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String masjidInformation =
                        "Masjid Username: " + edtUsername.getText().toString() + "\n" +
                                "Masjid Name: " + edtMasjidName.getText().toString() + "\n" +
                                "Masjid Phone: " + edtContactNumber.getText().toString() + "\n" +
                                "Masjid Address: " + edtAddress.getText().toString() + "\n" +
                                "Masjid Description: " + edtDescription.getText().toString() + "\n" +
                                "Masjid Latitude: " + masjidLat + "\n" +
                                "Masjid Longitude: " + masjidLon + "\n" +
                                "Masjid Prayer Timings: " + prayer_time + "\n" +
                                "Masjid Prayer Names: " + prayer_name;

                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"dawah@calltoallah.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Customer Support");
                intent.putExtra(Intent.EXTRA_TEXT, masjidInformation);
                intent.setType("text/html");
                intent.setPackage("com.google.android.gm");
                startActivity(Intent.createChooser(intent, "Send mail"));

                dialog.dismiss();
                activity.startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
    }

    private void Init(View v) {
        progressbarManager = new ProgressbarManager(getActivity());

        edtMasjidName = (EditText) v.findViewById(R.id.edtMasjidName);
        edtAddress = (EditText) v.findViewById(R.id.edtAddress);
        edtContactNumber = (EditText) v.findViewById(R.id.edtContactNumber);
        edtMasjidIcon = (EditText) v.findViewById(R.id.edtMasjidIcon);
        edtDescription = (EditText) v.findViewById(R.id.edtDescription);
        edtUsername = (EditText) v.findViewById(R.id.edtUsername);
        edtTimeTable = (EditText) v.findViewById(R.id.edtTimeTable);

        btnAddMasjid = (Button) v.findViewById(R.id.btnAddMasjid);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.gc();

        if (requestCode == SimplePlacePicker.SELECT_LOCATION_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                masjidLat = data.getDoubleExtra(SimplePlacePicker.LOCATION_LAT_EXTRA, -1);
                masjidLon = data.getDoubleExtra(SimplePlacePicker.LOCATION_LNG_EXTRA, -1);
                edtAddress.setText(data.getStringExtra(SimplePlacePicker.SELECTED_ADDRESS));
            }
        }

//        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
//            Place place = PingPlacePicker.getPlace(data);
//            if (place != null) {
//                masjidLat = place.getLatLng().latitude;
//                masjidLon = place.getLatLng().longitude;
//                edtAddress.setText(place.getAddress());
//            }
//        }

        if (requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_PICK_PHOTO) {
            if (data != null) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imagePath = cursor.getString(columnIndex);
                edtMasjidIcon.setText(imagePath);
//                previewBanner.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                cursor.close();
            }

        } else if (requestCode == CAMERA_PIC_REQUEST) {
            if (Build.VERSION.SDK_INT > 21) {
//                Glide.with(getActivity()).load(mImageFileLocation).into(previewBanner);
                imagePath = mImageFileLocation;
                edtMasjidIcon.setText(imagePath);

            } else {
//                Glide.with(getActivity()).load(fileUri).into(previewBanner);
                imagePath = fileUri.getPath();
                edtMasjidIcon.setText(imagePath);
            }
        }
    }

    private float ConvertDMSToDD(float degree, float minutes, float seconds, String direction) {
        float dd = degree + minutes / 60 + seconds / (60 * 60);

        if (direction == "S" || direction == "W") {
            dd = dd * -1;
        } // Don't do anything for N or E
        return dd;
    }

    private void captureImage() {
        if (Build.VERSION.SDK_INT > 21) { //use this if Lollipop_Mr1 (API 22) or above
            Intent callCameraApplicationIntent = new Intent();
            callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

            // We give some instruction to the intent to save the image
            File photoFile = null;

            // If the createImageFile will be successful, the photo file will have the address of the file
            photoFile = createImageFile();
            // Here we call the function that will try to catch the exception made by the throw function
            // Here we add an extra file to the intent to put the address on to. For this purpose we use the FileProvider, declared in the AndroidManifest.
            Uri outputUri = FileProvider.getUriForFile(
                    getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    photoFile);
            callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);

            // The following is a new line with a trying attempt
            callCameraApplicationIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Logger.getAnonymousLogger().info("Calling the camera App by intent");

            // The following strings calls the camera app and wait for his file in return.
            startActivityForResult(callCameraApplicationIntent, CAMERA_PIC_REQUEST);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

            // start the image capture Intent
            startActivityForResult(intent, CAMERA_PIC_REQUEST);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Directory Error: ", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    private File createImageFile() {
        Logger.getAnonymousLogger().info("Generating the image - method started");

        // Here we create a "non-collision file name", alternatively said, "an unique filename" using the "timeStamp" functionality
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmSS").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp;
        // Here we specify the environment location and the exact path where we want to save the so-created file
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/photo_saving_app");
        Logger.getAnonymousLogger().info("Storage directory set");

        // Then we create the storage directory if does not exists
        if (!storageDirectory.exists()) storageDirectory.mkdir();

        // Here we create the file using a prefix, a suffix and a directory
        File image = new File(storageDirectory, imageFileName + ".jpg");
        // File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        // Here the location is saved into the string mImageFileLocation
        Logger.getAnonymousLogger().info("File name and path set");

        mImageFileLocation = image.getAbsolutePath();
        // fileUri = Uri.parse(mImageFileLocation);
        // The file is returned to the previous intent across the camera application
        return image;
    }

    private void startMapActivity(String apiKey, String country, String language, String[] supportedAreas) {
        Intent intent = new Intent(getActivity(), MapActivity.class);
        Bundle bundle = new Bundle();

        bundle.putString(SimplePlacePicker.API_KEY, apiKey);
        bundle.putString(SimplePlacePicker.COUNTRY, country);
        bundle.putString(SimplePlacePicker.LANGUAGE, language);
        bundle.putStringArray(SimplePlacePicker.SUPPORTED_AREAS, supportedAreas);

        intent.putExtras(bundle);
        startActivityForResult(intent, SimplePlacePicker.SELECT_LOCATION_REQUEST_CODE);
    }
}

