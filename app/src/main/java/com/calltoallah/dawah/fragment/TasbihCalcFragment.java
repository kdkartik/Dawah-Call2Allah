package com.calltoallah.dawah.fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.ArcProgress;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.model.ErrorModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.Reader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.MainActivity.mBottomNavigationView;

public class TasbihCalcFragment extends BaseFragment {

    private Vibrator mVibrator;
    private boolean isTurnVibrator = true;
    private ArcProgress arcProgress;
    private ImageView imgVibrate, imgReset, imgSave;
    private int count = 0;
    private boolean responseFailed = false;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setActivity((MainActivity) getActivity());
        return inflater.inflate(R.layout.fragment_tasbih_calc, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        Init(v);
        setData();
    }

    private void setData() {

        imgVibrate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                mVibrator.vibrate(100);
                if (isTurnVibrator) {
                    isTurnVibrator = false;
                    imgVibrate.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorBlack)));
                } else {
                    isTurnVibrator = true;
                    imgVibrate.setImageTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                }
            }
        });

        imgReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View parentView = getLayoutInflater().inflate(R.layout.dialog_tasbih, null);
                final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                dialog.setContentView(parentView);
                dialog.show();

                TextView txtTotalCount = (TextView) dialog.findViewById(R.id.txtTotalCount);
                final EditText edtTasbihName = (EditText) dialog.findViewById(R.id.edtTasbihName);
                Button tasbihSubmit = (Button) dialog.findViewById(R.id.tasbihSubmit);
                Button tasbihCancel = (Button) dialog.findViewById(R.id.tasbihCancel);

                txtTotalCount.setText(getString(R.string.tasbih_count) + count);
                tasbihSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtTasbihName.getText().toString().equalsIgnoreCase("") && count == 0) {
                            Toast.makeText(getActivity(), getString(R.string.tasbih_name), Toast.LENGTH_SHORT).show();
                        } else {
                            addTasbih(count, edtTasbihName.getText().toString());
                        }
                        dialog.dismiss();
                    }
                });

                tasbihCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
            }
        });

        arcProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTurnVibrator) {
                    mVibrator.vibrate(100);
                }

                if (count == 100) {
                    View parentView = getLayoutInflater().inflate(R.layout.dialog_tasbih, null);
                    final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                    dialog.setContentView(parentView);
                    dialog.show();

                    TextView txtTotalCount = (TextView) dialog.findViewById(R.id.txtTotalCount);
                    final EditText edtTasbihName = (EditText) dialog.findViewById(R.id.edtTasbihName);
                    Button tasbihSubmit = (Button) dialog.findViewById(R.id.tasbihSubmit);
                    Button tasbihCancel = (Button) dialog.findViewById(R.id.tasbihCancel);

                    txtTotalCount.setText(getString(R.string.tasbih_count) + count);
                    tasbihSubmit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (edtTasbihName.getText().toString().equalsIgnoreCase("") && count == 0) {
                                Toast.makeText(getActivity(), getString(R.string.tasbih_name), Toast.LENGTH_SHORT).show();
                            } else {
                                addTasbih(count, edtTasbihName.getText().toString());
                            }
                            dialog.dismiss();
                        }
                    });

                    tasbihCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                } else {
                    count++;
                }

                arcProgress.setProgress(count);
                arcProgress.setText(count + "");
            }
        });
    }

    private void reset() {
        if (isTurnVibrator) {
            mVibrator.vibrate(100);
        }
        count = 0;
        arcProgress.setProgress(count);
        arcProgress.setText(count + "");
    }

    public void addTasbih(int tasbih_count, String tasbih_string) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<ResponseBody> apiCall = apiInterface.addTasbih(UserPreferences.loadUser(getActivity()).getId(),
                String.valueOf(tasbih_count), tasbih_string, dateFormat.format(calendar.getTime()));
        apiCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {

                    try {
                        String res = response.body().string();

                        try {
                            Gson gson = new Gson();
                            Reader reader = new StringReader(res);
                            ErrorModel errorModel = gson.fromJson(reader, ErrorModel.class);

                            if (errorModel.getStatus().equalsIgnoreCase("1")) {
                                Toast.makeText(activity, errorModel.getMessage(), Toast.LENGTH_SHORT).show();
                                reset();
                            }

                        } catch (IllegalStateException | JsonSyntaxException exception) {
                            responseFailed = true;
                        }

                        if (responseFailed) {
                            Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getActivity(), getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Init(View v) {
        arcProgress = (ArcProgress) v.findViewById(R.id.progressBar);
        imgVibrate = (ImageView) v.findViewById(R.id.imgVibrate);
        imgReset = (ImageView) v.findViewById(R.id.imgReset);
        imgSave = (ImageView) v.findViewById(R.id.imgSave);

        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public void onBack() {
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.container);
        activity.removeFragment(fragment);
    }
}

