package com.calltoallah.dawah.adapter;

import android.content.res.ColorStateList;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.TransectionListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<TransectionListModel> mItem;
    private int walletAmount = 0;

    public TransactionAdapter(MainActivity context, List<TransectionListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_transaction, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final TransectionListModel transectionListModel = mItem.get(position);

            if (transectionListModel.getAmountStatus().equals("")) {
                viewHolders.txtTransactionName.setText("Payment Failure");
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorYellow)));

            } else if (transectionListModel.getAmountStatus().equals("zakat")) {
                viewHolders.txtTransactionName.setText("Zakat Paid");
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorError)));

            } else if (transectionListModel.getAmountStatus().equals("verification_bonus")) {
                viewHolders.txtTransactionName.setText("Verification Bonus");
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));

            } else if (transectionListModel.getAmountStatus().equals("refferal")) {
                viewHolders.txtTransactionName.setText("Dawah Referring Bonus");
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));

            } else if (transectionListModel.getAmountStatus().equals("post_liked")) {
                viewHolders.txtTransactionName.setText(Html.fromHtml("Liked <b><font color="
                        + context.getResources().getColor(R.color.colorPrimaryLight) + "> "
                        + transectionListModel.getMasjidName() + "</font></b> Post"));
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorAccent)));

            } else if (transectionListModel.getAmountStatus().equals("direct_payment")) {
                viewHolders.txtTransactionName.setText(Html.fromHtml("Paid to <b><font color="
                        + context.getResources().getColor(R.color.colorPrimaryLight) + "> "
                        + transectionListModel.getMasjidName() + "</font></b>"));
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorError)));

            } else if (transectionListModel.getAmountStatus().equals("direct_payment_post")) {
                viewHolders.txtTransactionName.setText(Html.fromHtml("Paid to <b><font color="
                        + context.getResources().getColor(R.color.colorPrimaryLight) + "> "
                        + transectionListModel.getMasjidName() + "</font></b> Post"));
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorError)));

            } else if (transectionListModel.getAmountStatus().equals("added")) {
                viewHolders.txtTransactionName.setText(Html.fromHtml("Added to <b><font color="
                        + context.getResources().getColor(R.color.colorPrimaryLight) + "> "
                        + "Dawah Account" + "</font></b>"));
                viewHolders.txtTransactionAmount.setText("₹ " + transectionListModel.getAmount());
                viewHolders.txtTransactionAmount.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorPrimaryLight)));
            }

            if (transectionListModel.getCreated_on() != null) {
                String inputPattern = "yyyy-MM-dd hh:mm:ss";
                String outputPattern = "dd MMM, yyyy - hh:mm a";
                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                try {
                    Date date = inputFormat.parse(transectionListModel.getCreated_on());
                    String createdDate = outputFormat.format(date);
                    viewHolders.txtTransactionDate.setText(createdDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtTransactionName, txtTransactionDate, txtTransactionAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTransactionName = itemView.findViewById(R.id.txtTransactionName);
            txtTransactionDate = itemView.findViewById(R.id.txtTransactionDate);
            txtTransactionAmount = itemView.findViewById(R.id.txtTransactionAmount);
        }
    }

    public int getWalletTotal(List<TransectionListModel> items) {
        int totalPrice = 0;
        for (TransectionListModel transectionListModel : items) {
            if (transectionListModel.getAmountStatus().equalsIgnoreCase("verification_bonus") ||
                    transectionListModel.getAmountStatus().equalsIgnoreCase("refferal") ||
                    transectionListModel.getAmountStatus().equalsIgnoreCase("post_liked"))
                totalPrice += Integer.parseInt(transectionListModel.getAmount());
        }
        return totalPrice;
    }
}
