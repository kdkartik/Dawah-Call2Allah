package com.calltoallah.dawah.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.CircularImageView;
import com.calltoallah.dawah.model.FollowerListModel;

import java.util.List;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class AvatarAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<FollowerListModel> mItem;
    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public AvatarAdapter(MainActivity context, List<FollowerListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_avatar, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder holder = (ViewHolder) viewHolder;

            final FollowerListModel followerListModel = mItem.get(position);

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + followerListModel.getUserAvatar())
                    .apply(options)
                    .into(holder.imageView);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mItem != null && mItem.size() > 4) {
            return 4;
        } else {
            return mItem.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected CircularImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView
                    .findViewById(R.id.profile_image);
        }
    }
}
