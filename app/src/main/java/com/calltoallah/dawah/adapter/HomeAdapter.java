package com.calltoallah.dawah.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.icu.util.IslamicCalendar;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.calltoallah.dawah.DonationActivity;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.comman.ArcProgress;
import com.calltoallah.dawah.comman.Constants;
import com.calltoallah.dawah.comman.UserPreferences;
import com.calltoallah.dawah.firebase.SendNotification;
import com.calltoallah.dawah.fragment.MasjidDetailsFragment;
import com.calltoallah.dawah.fragment.PostFragment;
import com.calltoallah.dawah.fragment.UserVerificationFragment;
import com.calltoallah.dawah.model.HomeMasjidPostList;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.model.MasjidModel;
import com.calltoallah.dawah.model.RecyclerViewItem;
import com.calltoallah.dawah.model.TimeTableModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;
import com.calltoallah.dawah.notification.AlarmService;
import com.calltoallah.dawah.notification.LogTag;
import com.calltoallah.dawah.notification.SharePreferenceManager;
import com.calltoallah.dawah.notification.SharePreferenceManager.CONST;
import com.calltoallah.dawah.notification.database.DbManager;
import com.calltoallah.dawah.notification.database.MasjidMaster;
import com.calltoallah.dawah.notification.database.NamazMaster;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Reader;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;


public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private MainActivity context;
    private List<HomeMasjidPostList> postList;
    private List<RecyclerViewItem> mediaObjects;
    private PostFilter mFilter;
    private String mSearchText = "";

    // For Alarm ...
    private long upcomingNamazTime = 0;
    private long progress = 0;

    private static final int HEADER_ITEM = 0;
    private static final int FOOTER_ITEM = 1;

    public HomeAdapter(MainActivity ctx, List<RecyclerViewItem> mediaObjects, List<HomeMasjidPostList> postList) {
        this.context = ctx;
        this.postList = postList;
        this.mediaObjects = mediaObjects;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View row;

        if (viewType == HEADER_ITEM) {
            row = inflater.inflate(R.layout.item_header, viewGroup, false);
            return new HeaderHolder(row);
        } else if (viewType == FOOTER_ITEM) {
            row = inflater.inflate(R.layout.item_home, viewGroup, false);
            return new FooterHolder(row);
        }

        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        RecyclerViewItem recyclerViewItem = mediaObjects.get(position);

        if (holder instanceof HeaderHolder) {
            HeaderHolder headerHolder = (HeaderHolder) holder;
            MasjidListModel nearbyObject = (MasjidListModel) recyclerViewItem;
            //set data
            configureDashboard(headerHolder, nearbyObject);

        } else if (holder instanceof FooterHolder) {
            FooterHolder footerHolder = (FooterHolder) holder;
            HomeMasjidPostList homeMasjidPostList = (HomeMasjidPostList) recyclerViewItem;
            //set data
            configurePosts(footerHolder, homeMasjidPostList, position);
        }
    }

    private void configurePosts(FooterHolder holder, HomeMasjidPostList mediaObject, int position) {
        if (mediaObject.getDocPath().equalsIgnoreCase("")) {
            holder.media_container.setVisibility(View.GONE);
            holder.txtPostInfo.setBackgroundResource(R.color.colorPrimary);
            holder.txtPostInfo.setTextColor(Color.WHITE);
            holder.txtPostInfo.setGravity(Gravity.CENTER);

            ViewGroup.LayoutParams params = holder.txtPostInfo.getLayoutParams();
            params.height = 400;
            holder.txtPostInfo.setLayoutParams(params);

        } else {
            holder.media_container.setVisibility(View.VISIBLE);
            holder.txtPostInfo.setBackgroundResource(R.color.colorTransparent);
            holder.txtPostInfo.setTextColor(Color.BLACK);
            holder.txtPostInfo.setGravity(Gravity.LEFT);

            ViewGroup.LayoutParams params = holder.txtPostInfo.getLayoutParams();
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            holder.txtPostInfo.setLayoutParams(params);

            Glide.with(context)
                    .load(API_DOMAIN + mediaObject.getDocPath())
                    .into(holder.thumbnail);
        }

        // For Danation ...
        Log.d("donation_button: ", mediaObject.getDonationButton() + "");

        if (mediaObject.getDonationButton().equalsIgnoreCase("1")) {
            holder.donationContainer.setVisibility(View.VISIBLE);

            int userDonated = Integer.parseInt(mediaObject.getUser_donated());
            int userDonatedAmt = Integer.parseInt(mediaObject.getAsked_donation_amount());

            holder.progressDonation.setMax(userDonatedAmt);
            holder.progressDonation.setProgress(userDonated);
            holder.txtDonationAmt.setText("₹ " + mediaObject.getUser_donated() + " Collected from the target of ₹ " + mediaObject.getAsked_donation_amount());

            if (userDonated >= userDonatedAmt) {
                holder.txtDonate.setText("Donated");
                holder.txtDonate.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorGray)));
                holder.txtDonate.setEnabled(false);
            } else {
                holder.txtDonate.setEnabled(true);
            }

        } else {
            holder.donationContainer.setVisibility(View.GONE);
            holder.txtDonate.setBackgroundTintList(ColorStateList.valueOf(context.getResources().getColor(R.color.colorGray)));
            holder.txtDonate.setEnabled(false);
        }

        holder.txtLikeCount.setText(prettyCount(Integer.parseInt(mediaObject.getLike_count())));
        holder.txtShareCount.setText(prettyCount(Integer.parseInt(mediaObject.getShare_count())));

        if (mSearchText != null && !mSearchText.isEmpty()) {
            // Masjid name ...
            int startName = mediaObject.getMasjidName().toLowerCase().indexOf(mSearchText.toLowerCase());
            int endName = startName + mSearchText.length();

            if (startName != -1) {
                Spannable spannable = new SpannableString(mediaObject.getMasjidName());
                spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startName, endName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.txtMasjidName.setText(spannable);
            } else {
                holder.txtMasjidName.setText(mediaObject.getMasjidName());
            }

            // Post Text  ...
            int startText = mediaObject.getPostText().toLowerCase().indexOf(mSearchText.toLowerCase());
            int endText = startText + mSearchText.length();

            if (startText != -1) {
                Spannable spannable = new SpannableString(mediaObject.getPostText());
                spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#A9A9A9")), startText, endText, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.txtPostInfo.setText(spannable);
            } else {
                holder.txtPostInfo.setText(mediaObject.getPostText());
            }

        } else {
            holder.txtMasjidName.setText(mediaObject.getMasjidName());
            holder.txtPostInfo.setText(mediaObject.getPostText());
        }

        if (mediaObject.getVerified_status() != null) {
            if (mediaObject.getVerified_status().equalsIgnoreCase("1")) {
                holder.txtMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                holder.txtMasjidName.setCompoundDrawablePadding(5);
            }
        }

        if (mediaObject.getCreatedOn() != null) {
            String inputPattern = "yyyy-MM-dd HH:mm:ss";
            String outputPattern = "EEEE, dd MMMM, yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

            try {
                Date date = inputFormat.parse(mediaObject.getCreatedOn());
                String createdDate = outputFormat.format(date);
                holder.txtCreatedDate.setText(createdDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Glide.with(context)
                .load(API_DOMAIN + mediaObject.getMasjidImage())
                .into(holder.imgMasjidIcon);

        holder.masjidContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMasjidDetails(mediaObject.getMasjidId());
            }
        });

        holder.txtPostInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 15);
                bundle.putParcelable(Constants.DATA, mediaObject);

                PostFragment postFragment = new PostFragment();
                postFragment.setArguments(bundle);
                context.loadFragment(postFragment);
            }
        });

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.MODE, 15);
                bundle.putParcelable(Constants.DATA, mediaObject);

                PostFragment postFragment = new PostFragment();
                postFragment.setArguments(bundle);
                context.loadFragment(postFragment);
            }
        });

        if (mediaObject.isPost_liked() == true) {
            holder.postLike.setImageResource(R.drawable.ic_action_like);
        } else {
            holder.postLike.setImageResource(R.drawable.ic_action_unlike);
        }

        holder.postLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                final Call<String> loginCall = apiInterface.getPostsLikeUnlike(mediaObject.getMasjidId(),
                        mediaObject.getId(), UserPreferences.loadUser(context).getId());
                loginCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {

                            try {
                                JSONObject jsonObject = new JSONObject(response.body().toString());
                                if (jsonObject.getString("message").equalsIgnoreCase("Post liked")) {
                                    int likeCount = Integer.parseInt(mediaObject.getLike_count()) + 1;
                                    mediaObject.setLike_count(String.valueOf(likeCount));
                                    mediaObject.setPost_liked(true);

                                    SendNotification.getUserNotificationToken(mediaObject.getUser_id(),
                                            context.getString(R.string.app_name),
                                            UserPreferences.loadUser(context).getFullName() + " Liked your Post", context);

                                } else {
                                    int likeCount = Integer.parseInt(mediaObject.getLike_count()) - 1;
                                    mediaObject.setLike_count(String.valueOf(likeCount));
                                    mediaObject.setPost_liked(false);
                                }
                                ;

                                notifyItemChanged(position);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            }
        });

        holder.postShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
                final Call<String> loginCall = apiInterface.getPostsShare(
                        mediaObject.getMasjidId(), mediaObject.getId(),
                        UserPreferences.loadUser(context).getId());
                loginCall.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            Log.d("SHARE: ", response.body().toString());

                            try {
                                JSONObject jsonObject = new JSONObject(response.body().toString());
                                String message = jsonObject.getString("message");

                                String shareBody = mediaObject.getMasjidName() + "\n\n" +
                                        "Get this masjid/madrasah post from below link:" + "\n" +
                                        mediaObject.getQr_code() + "/post/" + mediaObject.getId();
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
                                context.startActivity(Intent.createChooser(sharingIntent, context.getResources().getString(R.string.app_name)));

                                int shareCount = Integer.parseInt(mediaObject.getShare_count()) + 1;
                                mediaObject.setShare_count(String.valueOf(shareCount));

                                SendNotification.getUserNotificationToken(mediaObject.getUser_id(),
                                        context.getString(R.string.app_name),
                                        UserPreferences.loadUser(context).getFullName() + " Shared your Post", context);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            notifyItemChanged(position);
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                    }
                });
            }
        });

        holder.txtDonate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (UserPreferences.loadUser(context).getVerify_status().equalsIgnoreCase("1")) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.MODE, 0);
                    bundle.putParcelable(Constants.DATA, mediaObject);

                    Intent intent = new Intent(context, DonationActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);

                } else {
                    context.loadFragment(new UserVerificationFragment());
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        RecyclerViewItem recyclerViewItem = mediaObjects.get(position);

        if (recyclerViewItem instanceof MasjidListModel)
            return HEADER_ITEM;
        else if (recyclerViewItem instanceof HomeMasjidPostList)
            return FOOTER_ITEM;
        else
            return super.getItemViewType(position);

    }

    private class HeaderHolder extends RecyclerView.ViewHolder {
        private TextView txtMute, txtWelcomeText, txtCurrentDateTime, txtShowAllTimes, txtIslamicDate;
        private CardView showTimes;
        private TextView txtMasjidNames, txtMasjidAddress, txtNamazName, txtNamazTime, txtNamazRemaining;
        private ArcProgress masjidProgress;
        private RecyclerView mTimeTable;

        HeaderHolder(View itemView) {
            super(itemView);
            showTimes = (CardView) itemView.findViewById(R.id.showTimes);
            mTimeTable = (RecyclerView) itemView.findViewById(R.id.rcvTimeTable);
            mTimeTable.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            mTimeTable.setItemAnimator(new DefaultItemAnimator());

            txtMute = (TextView) itemView.findViewById(R.id.txtMute);
            txtIslamicDate = (TextView) itemView.findViewById(R.id.txtIslamicDate);
            txtShowAllTimes = (TextView) itemView.findViewById(R.id.txtShowAllTimes);
            txtWelcomeText = (TextView) itemView.findViewById(R.id.txtWelcomeText);
            txtCurrentDateTime = (TextView) itemView.findViewById(R.id.txtCurrentDateTime);
            txtMasjidNames = (TextView) itemView.findViewById(R.id.txtMasjidNames);
            txtMasjidAddress = (TextView) itemView.findViewById(R.id.txtMasjidAddress);
            txtNamazName = (TextView) itemView.findViewById(R.id.txtNamazName);
            txtNamazTime = (TextView) itemView.findViewById(R.id.txtNamazTime);
            txtNamazRemaining = (TextView) itemView.findViewById(R.id.txtNamazRemaining);
            masjidProgress = (ArcProgress) itemView.findViewById(R.id.masjidProgress);
        }
    }

    public class FooterHolder extends RecyclerView.ViewHolder {

        // Post ...
        private FrameLayout media_container;
        private LinearLayout donationContainer, masjidContainer;
        public ProgressBar progressDonation;
        private TextView txtPostInfo, txtMasjidName, txtCreatedDate, txtDonate, txtDonationAmt, txtLikeCount, txtShareCount;
        private ImageView thumbnail, postLike, postShare, imgMasjidIcon, imgPostUpdate;

        FooterHolder(View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            media_container = itemView.findViewById(R.id.media_container);
            masjidContainer = itemView.findViewById(R.id.masjidContainer);
            txtLikeCount = (TextView) itemView.findViewById(R.id.txtLikeCount);
            txtShareCount = (TextView) itemView.findViewById(R.id.txtShareCount);

            // Post ...
            txtPostInfo = itemView.findViewById(R.id.txtPostInfo);
            txtMasjidName = itemView.findViewById(R.id.txtMasjidName);
            txtDonate = itemView.findViewById(R.id.txtDonate);
            txtCreatedDate = itemView.findViewById(R.id.txtCreatedDate);
            postLike = itemView.findViewById(R.id.postLike);
            postShare = itemView.findViewById(R.id.postShare);
            imgMasjidIcon = itemView.findViewById(R.id.imgMasjidIcon);
            txtDonationAmt = itemView.findViewById(R.id.txtDonationAmt);
            donationContainer = itemView.findViewById(R.id.donationContainer);
            imgPostUpdate = itemView.findViewById(R.id.imgPostUpdate);
            imgPostUpdate.setVisibility(View.GONE);

            progressDonation = (ProgressBar) itemView.findViewById(R.id.progressDonation);
            txtDonationAmt.setSelected(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void configureDashboard(HeaderHolder holder, MasjidListModel masjidListModel) {

        if (masjidListModel.getPrayerTime() != null) {

            setWelcomeMessageTime(holder);
            setIslamicDateTime(holder);
            setEventListener(holder);

            List<String> prayerTime = Arrays.asList(masjidListModel.getPrayerTime().split(","));
            List<String> prayerName = Arrays.asList(masjidListModel.getPrayerName().split(","));

            if (prayerTime != null && prayerName != null) {

                List<String> mFilterTime = new ArrayList<>();
                List<String> mFilterName = new ArrayList<>();
                List<TimeTableModel> timeTableModelList = new ArrayList<>();
                for (int i = 0; i < prayerTime.size(); i++) {

                    if (!prayerTime.get(i).contains("Optional")) {
                        TimeTableModel timeTableModel = new TimeTableModel();
                        timeTableModel.setTt_time(prayerTime.get(i).trim());
                        timeTableModel.setTt_name(prayerName.get(i).trim());
                        timeTableModelList.add(timeTableModel);

                        mFilterTime.add(prayerTime.get(i).trim());
                        mFilterName.add(prayerName.get(i).trim());
                    }
                }

                TimeTableAdapter adapter = new TimeTableAdapter(context, timeTableModelList);
                holder.mTimeTable.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                String[] namazTime = mFilterTime.toArray(new String[mFilterTime.size()]);
                String[] namazName = mFilterName.toArray(new String[mFilterName.size()]);
                rescheduleNotifications(namazTime, namazName, masjidListModel);

                // Register receiver for Upcoming events or notification.
                context.registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        NamazMaster namazMaster = (NamazMaster) intent.getSerializableExtra("namaz");

                        MasjidMaster masjidMaster = (MasjidMaster) intent.getSerializableExtra("masjid");
                        DateFormat sdfInput = new SimpleDateFormat("hh:mm a");
                        holder.txtNamazTime.setText(sdfInput.format(namazMaster.namaz_time));
                        holder.txtNamazName.setText(namazMaster.namaz_name);
                        holder.txtMasjidNames.setText(masjidMaster.name);
                        holder.txtMasjidAddress.setText(masjidMaster.address);

                        LogTag.d("Upcoming Event: Namaz -> " + namazMaster + " Masjid -> " + masjidMaster);
                        upcomingNamazTime = namazMaster.namaz_time;
                        long totalDelay = namazMaster.namaz_time - Calendar.getInstance().getTimeInMillis();
                        int days = (int) (totalDelay / (1000 * 60 * 60 * 24));
                        int hours = (int) ((totalDelay - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                        int min = (int) (totalDelay - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                        int sec = (int) (totalDelay - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours) - (1000 * 60 * min)) / (1000);
                        LogTag.d("Upcoming Event Fire after -> Days: " + days + "Hours: " + hours + ", Mins: " + min + ", Secs: " + sec);

                        holder.txtNamazRemaining.setText(String.format("%02d:%02d:%02d",
                                hours, min, sec));

                        progress = totalDelay;
                        holder.masjidProgress.setMax((int) (totalDelay / 1000));
                        holder.masjidProgress.setText("");

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                long totalDelay = upcomingNamazTime - Calendar.getInstance().getTimeInMillis();
                                if (totalDelay > 0) {
                                    int days = (int) (totalDelay / (1000 * 60 * 60 * 24));
                                    int hours = (int) ((totalDelay - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                                    int min = (int) (totalDelay - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                                    int sec = (int) (totalDelay - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours) - (1000 * 60 * min)) / (1000);

                                    holder.txtNamazRemaining.setText(String.format("%02d:%02d:%02d",
                                            hours, min, sec));

                                    holder.masjidProgress.setProgress((int) (progress - totalDelay) / 1000);
                                    holder.masjidProgress.setText("");

                                    if (totalDelay >= 1000)
                                        new Handler().postDelayed(this, 1000);
                                    else
                                        new Handler().postDelayed(this, totalDelay);
                                }
                            }
                        }, 1000);

                    }
                }, new IntentFilter("com.upcoming_event"));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mediaObjects == null ? 0 : mediaObjects.size();
    }

    public String prettyCount(long numValue) {
        char[] suffix = {' ', 'K', 'M', 'B', 'T', 'P', 'E'};
        int value = (int) Math.floor(Math.log10(numValue));
        int base = value / 3;
        if (value >= 3 && base < suffix.length) {
            return new DecimalFormat("#0.0").format(numValue / Math.pow(10, base * 3)) + suffix[base];
        } else {
            return new DecimalFormat("#,##0").format(numValue);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void setIslamicDateTime(HeaderHolder holder) {
        Calendar gregorianCal = Calendar.getInstance();
        IslamicCalendar cal = new IslamicCalendar();
        cal.setTime(gregorianCal.getTime());

        int month = cal.get(java.util.Calendar.MONTH);
        String day = String.format("%02d", cal.get(java.util.Calendar.DAY_OF_MONTH));
        String year = String.format("%02d", cal.get(java.util.Calendar.YEAR));
        holder.txtIslamicDate.setText(getMonth(month) + " - " + day + ", " + year);
    }

    private void setWelcomeMessageTime(HeaderHolder holder) {
        holder.txtWelcomeText.setText("Welcome " + UserPreferences.loadUser(context).getFullName());

        String outputPattern = "EEEE, dd MMMM, yyyy";
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        String createdDate = outputFormat.format(new Date());
        holder.txtCurrentDateTime.setText(createdDate);
    }

    private void setEventListener(HeaderHolder holder) {
        holder.txtShowAllTimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.showTimes.getVisibility() == View.VISIBLE) {
                    holder.showTimes.setVisibility(View.GONE);
                } else {
                    holder.showTimes.setVisibility(View.VISIBLE);
                }
            }
        });

        boolean isMute = SharePreferenceManager.getInstance(context).getSharePreference().getBoolean(CONST.IS_MUTE, false);
        if (isMute == false) {
            holder.txtMute.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_mute, 0, 0, 0);
        } else {
            holder.txtMute.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_unmute, 0, 0, 0);
        }

        holder.txtMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isMute = SharePreferenceManager.getInstance(context).getSharePreference().getBoolean(CONST.IS_MUTE, false);
                if (isMute == false) {
                    holder.txtMute.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_unmute, 0, 0, 0);
                    SharePreferenceManager.getInstance(context).getEditor().putBoolean(CONST.IS_MUTE, true).commit();

                } else {
                    holder.txtMute.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_mute, 0, 0, 0);
                    SharePreferenceManager.getInstance(context).getEditor().putBoolean(CONST.IS_MUTE, false).commit();
                }
            }
        });
    }

    public void rescheduleNotifications(String[] namazTime, String[] namazName, MasjidListModel masjidListModel) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                LogTag.d("RescheduleNotifications:");
                DbManager dbManager = DbManager.getInstance();
                dbManager.deleteAllNamaz(); // Delete all namaz information before add new information.
                dbManager.deleteAllMasjid(); // Delete all masjid information before add new information

                // Insert Masjid information in local database.
                MasjidMaster masjidMaster = new MasjidMaster();
                masjidMaster.name = masjidListModel.getMasjidName();
                masjidMaster.address = masjidListModel.getAddress();
                masjidMaster.image = masjidListModel.getImagePath();
                masjidMaster.latitude = Double.parseDouble(masjidListModel.getMasjidLat());
                masjidMaster.longitude = Double.parseDouble(masjidListModel.getMasjidLng());
                masjidMaster.uid = dbManager.insertMasjid(masjidMaster);
                LogTag.d("Inserted Masjid =" + masjidMaster);

//                Log.d("BN", "run: " + masjidMaster);
//                String[] namazTimings = {"11:32 PM", "11:35 PM", "11:37 PM", "11:40 PM", "11:43 PM"};
//                String[] namazNames = {"Fajar", "Zuhar", "Asar", "Maghrib", "Isha"};
                // Insert Namaz Information.
                int index = 0;
                for (String namaz_time : namazTime) {
                    try {
                        NamazMaster namazMaster = new NamazMaster();
                        namazMaster.masjid_id = masjidMaster.uid;
                        namazMaster.namaz_name = namazName[index++];
                        namazMaster.namaz_time = ConvertStringTimeToLong(namaz_time);
                        namazMaster.uid = dbManager.insertNamaz(namazMaster);
                        LogTag.d("Inserted Namaz = " + namazMaster);
                        String reGenerateNamazTime = SharePreferenceManager.getInstance(context).getSharePreference().getString(CONST.RE_GENERATE_NAMAZ_TIME, "");
                        LogTag.d("reGenerateNamazTime: " + reGenerateNamazTime);
                        if(!reGenerateNamazTime.trim().isEmpty()) {
                            // Handle Database ID changed scenarios. (On App Open remove old records and insert new records if Regenerate set then id mismatched.)
                            if(namaz_time.toLowerCase().equals(reGenerateNamazTime.toLowerCase())) {
                                SharePreferenceManager.getInstance(context).getEditor().putLong(CONST.RE_GENERATE_ID, namazMaster.uid).commit();
                                LogTag.d("Id Reset");
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                LogTag.d("After RescheduleNotifications:");
                {
                    List<MasjidMaster> masjidMasters = DbManager.getInstance().getAllMasjid();
                    for (MasjidMaster master : masjidMasters) {
                        LogTag.d("Masjid:" + master);
                    }
                    List<NamazMaster> namazMasters = DbManager.getInstance().getAllNamaz();
                    for (NamazMaster master : namazMasters) {
                        LogTag.d("Namaz:" + master);
                    }
                }

                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // After database update - If service is stopped then start server. If service is started then restart service.
                        if (AlarmService.isInstanceCreated()) {
                            context.stopService(new Intent(context, AlarmService.class));
                        } else {
                            context.startService(new Intent(context, AlarmService.class));
                        }
                    }
                });
            }
        }).start();
    }

    private long ConvertStringTimeToLong(String time) throws ParseException {

        SimpleDateFormat parsFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = parsFormat.parse(dateFormat.format(new Date()) + " " + time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // If namaz time is already gone then set namaz time for next day.
        if (Calendar.getInstance().getTimeInMillis() >= calendar.getTimeInMillis()) //
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTimeInMillis();
    }

    private String getMonth(int event_month) {
        String islamicMonth = "";
        if (event_month == 0) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[0];
        } else if (event_month == 1) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[1];
        } else if (event_month == 2) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[2];
        } else if (event_month == 3) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[3];
        } else if (event_month == 4) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[4];
        } else if (event_month == 5) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[5];
        } else if (event_month == 6) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[6];
        } else if (event_month == 7) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[7];
        } else if (event_month == 8) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[8];
        } else if (event_month == 9) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[9];
        } else if (event_month == 10) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[10];
        } else if (event_month == 11) {
            islamicMonth = context.getResources().getStringArray(R.array.custom_months)[11];
        }
        return islamicMonth;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new PostFilter(postList, this);
        }
        return mFilter;
    }

    public class PostFilter extends Filter {
        private HomeAdapter adapter;
        private List<HomeMasjidPostList> filterList;

        public PostFilter(List<HomeMasjidPostList> filterList, HomeAdapter adapter) {
            this.adapter = adapter;
            this.filterList = filterList;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint.toString().isEmpty()) {
                mSearchText = "";
            } else {
                mSearchText = (String) constraint;
            }

            if (constraint != null && constraint.length() > 0) {
                constraint = constraint.toString().toUpperCase();
                ArrayList<HomeMasjidPostList> filteredPlayers = new ArrayList<>();
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getMasjidName().toUpperCase().contains(constraint) ||
                            filterList.get(i).getPostText().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(filterList.get(i));
                    }
                }
                results.count = filteredPlayers.size();
                results.values = filteredPlayers;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            postList = (List<HomeMasjidPostList>) results.values;
            adapter.notifyDataSetChanged();
        }
    }

    private void getMasjidDetails(String masjidID) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> loginCall = apiInterface.getMasjidDetails(masjidID);
        loginCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {
                    Log.d("MASJID_DETAIL: ", response.body().toString());

                    try {
                        Gson gson = new Gson();
                        Reader reader = new StringReader(response.body().toString());
                        final MasjidModel masjidModel = gson.fromJson(reader, MasjidModel.class);

                        if (masjidModel.getMasjidListModel().size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putInt(Constants.MODE, 0);
                            bundle.putParcelable(Constants.DATA, masjidModel.getMasjidListModel().get(0));

                            MasjidDetailsFragment masjidDetailsFragment = new MasjidDetailsFragment();
                            masjidDetailsFragment.setArguments(bundle);
                            context.loadFragment(masjidDetailsFragment);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
}