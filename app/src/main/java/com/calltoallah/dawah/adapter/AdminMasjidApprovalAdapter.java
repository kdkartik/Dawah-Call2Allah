package com.calltoallah.dawah.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.calltoallah.dawah.MainActivity;
import com.calltoallah.dawah.R;
import com.calltoallah.dawah.model.MasjidListModel;
import com.calltoallah.dawah.network.APIClient;
import com.calltoallah.dawah.network.APIInterface;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.calltoallah.dawah.network.APIInterface.API_DOMAIN;

public class AdminMasjidApprovalAdapter extends RecyclerView.Adapter {

    private MainActivity context;
    private List<MasjidListModel> mItem;
    private boolean responseFailed = false;

    public AdminMasjidApprovalAdapter(MainActivity context, List<MasjidListModel> items) {
        this.context = context;
        this.mItem = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_masjid_approval, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (!mItem.isEmpty() && mItem != null) {
            final ViewHolder viewHolders = (ViewHolder) viewHolder;

            final MasjidListModel masjidListModel = mItem.get(position);
            viewHolders.txtMasjidName.setText(masjidListModel.getMasjidName());
            viewHolders.txtMasjidUsername.setText(masjidListModel.getMasjidUsername());

            if (masjidListModel.getVerified_status().equalsIgnoreCase("1")) {
                viewHolders.txtMasjidName.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_verified, 0);
                viewHolders.txtMasjidName.setCompoundDrawablePadding(5);
                viewHolders.txtMasjidName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            } else {
                viewHolders.txtMasjidName.setTextColor(context.getResources().getColor(R.color.colorError));
            }

            RequestOptions options = new RequestOptions()
                    .placeholder(R.drawable.no_profile_image)
                    .error(R.drawable.no_profile_image);

            Glide.with(context)
                    .load(API_DOMAIN + masjidListModel.getImagePath())
                    .apply(options)
                    .into(viewHolders.imgMasjid);

            viewHolders.imgApprove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    approveMasjid(masjidListModel.getId(), position);
                }
            });

            viewHolders.imgReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteMasjid(masjidListModel.getId(), position);
                }
            });
        }
    }

    private void approveMasjid(String id, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.addMasjidApprove(id);
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {

                            mItem.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, mItem.size());

                        } else {
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteMasjid(String id, int position) {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<String> apiCall = apiInterface.deleteMasjidRequest("1", id);
        apiCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (response.isSuccessful()) {

                    try {
                        String res = response.body().toString();
                        JSONObject jsonObject = new JSONObject(res);
                        if (jsonObject.getInt("status") == 1) {

                            mItem.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, mItem.size());

                        } else {
                            Toast.makeText(context, "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.unable_to_get_response), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected TextView txtMasjidName, txtMasjidUsername;
        protected ImageView imgMasjid, imgReject, imgApprove;

        public ViewHolder(View itemView) {
            super(itemView);
            txtMasjidName = itemView.findViewById(R.id.txtMasjidName);
            txtMasjidUsername = itemView.findViewById(R.id.txtMasjidUsername);

            imgMasjid = itemView.findViewById(R.id.imgMasjid);
            imgReject = itemView.findViewById(R.id.imgReject);
            imgApprove = itemView.findViewById(R.id.imgApprove);
        }
    }
}
