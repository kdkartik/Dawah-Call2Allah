package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TasbihEvent implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("tasbih_count")
    @Expose
    private String tasbihCount;
    @SerializedName("tasbih_name")
    @Expose
    private String tasbihName;
    @SerializedName("tasbih_timestamp")
    @Expose
    private String tasbihTimestamp;
    public final static Creator<TasbihEvent> CREATOR = new Creator<TasbihEvent>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TasbihEvent createFromParcel(Parcel in) {
            return new TasbihEvent(in);
        }

        public TasbihEvent[] newArray(int size) {
            return (new TasbihEvent[size]);
        }

    };

    protected TasbihEvent(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.tasbihCount = ((String) in.readValue((String.class.getClassLoader())));
        this.tasbihName = ((String) in.readValue((String.class.getClassLoader())));
        this.tasbihTimestamp = ((String) in.readValue((String.class.getClassLoader())));
    }

    public TasbihEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTasbihCount() {
        return tasbihCount;
    }

    public void setTasbihCount(String tasbihCount) {
        this.tasbihCount = tasbihCount;
    }

    public String getTasbihName() {
        return tasbihName;
    }

    public void setTasbihName(String tasbihName) {
        this.tasbihName = tasbihName;
    }

    public String getTasbihTimestamp() {
        return tasbihTimestamp;
    }

    public void setTasbihTimestamp(String tasbihTimestamp) {
        this.tasbihTimestamp = tasbihTimestamp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(tasbihCount);
        dest.writeValue(tasbihName);
        dest.writeValue(tasbihTimestamp);
    }

    public int describeContents() {
        return 0;
    }

}