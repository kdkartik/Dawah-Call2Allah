package com.calltoallah.dawah.model;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferModel implements Parcelable {

    @SerializedName("user_added_list")
    @Expose
    private List<ReferUserAddedListModel> userAddedList = null;
    public final static Parcelable.Creator<ReferModel> CREATOR = new Creator<ReferModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ReferModel createFromParcel(Parcel in) {
            return new ReferModel(in);
        }

        public ReferModel[] newArray(int size) {
            return (new ReferModel[size]);
        }

    };

    protected ReferModel(Parcel in) {
        in.readList(this.userAddedList, (ReferUserAddedListModel.class.getClassLoader()));
    }

    public ReferModel() {
    }

    public List<ReferUserAddedListModel> getUserAddedList() {
        return userAddedList;
    }

    public void setUserAddedList(List<ReferUserAddedListModel> userAddedList) {
        this.userAddedList = userAddedList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(userAddedList);
    }

    public int describeContents() {
        return 0;
    }

}