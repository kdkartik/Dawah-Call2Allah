package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasjidTransactionListModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("masjid_id")
    @Expose
    private String masjidId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("date_time")
    @Expose
    private String dateTime;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("post_id")
    @Expose
    private String postId;
    @SerializedName("custom_order_id")
    @Expose
    private String customOrderId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("full_name")
    @Expose
    private String full_name;
    public final static Parcelable.Creator<MasjidTransactionListModel> CREATOR = new Creator<MasjidTransactionListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MasjidTransactionListModel createFromParcel(Parcel in) {
            return new MasjidTransactionListModel(in);
        }

        public MasjidTransactionListModel[] newArray(int size) {
            return (new MasjidTransactionListModel[size]);
        }

    };

    protected MasjidTransactionListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidId = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
        this.dateTime = ((String) in.readValue((String.class.getClassLoader())));
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
        this.postId = ((String) in.readValue((String.class.getClassLoader())));
        this.customOrderId = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.full_name = ((String) in.readValue((Object.class.getClassLoader())));
    }

    public MasjidTransactionListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMasjidId() {
        return masjidId;
    }

    public void setMasjidId(String masjidId) {
        this.masjidId = masjidId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getCustomOrderId() {
        return customOrderId;
    }

    public void setCustomOrderId(String customOrderId) {
        this.customOrderId = customOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(masjidId);
        dest.writeValue(userId);
        dest.writeValue(amount);
        dest.writeValue(dateTime);
        dest.writeValue(orderId);
        dest.writeValue(postId);
        dest.writeValue(customOrderId);
        dest.writeValue(status);
        dest.writeValue(full_name);
    }

    public int describeContents() {
        return 0;
    }
}
