package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileModel implements Parcelable {

    @SerializedName("user_info")
    @Expose
    private List<ProfileUserInfo> profileUserInfo = null;
    public final static Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProfileModel createFromParcel(Parcel in) {
            return new ProfileModel(in);
        }

        public ProfileModel[] newArray(int size) {
            return (new ProfileModel[size]);
        }

    };

    protected ProfileModel(Parcel in) {
        in.readList(this.profileUserInfo, (ProfileUserInfo.class.getClassLoader()));
    }

    public ProfileModel() {
    }

    public List<ProfileUserInfo> getProfileUserInfo() {
        return profileUserInfo;
    }

    public void setProfileUserInfo(List<ProfileUserInfo> profileUserInfo) {
        this.profileUserInfo = profileUserInfo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(profileUserInfo);
    }

    public int describeContents() {
        return 0;
    }

}