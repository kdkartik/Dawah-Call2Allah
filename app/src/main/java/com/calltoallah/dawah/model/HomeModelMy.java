package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HomeModelMy  implements Parcelable {

    @SerializedName("masjid_post_list")
    @Expose
    private List<HomeMasjidPostList> homeMasjidPostList = new ArrayList<>();

    public final static Creator<HomeModelMy> CREATOR = new Creator<HomeModelMy>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeModelMy createFromParcel(Parcel in) {
            return new HomeModelMy(in);
        }

        public HomeModelMy[] newArray(int size) {
            return (new HomeModelMy[size]);
        }

    };

    protected HomeModelMy(Parcel in) {
        in.readList(this.homeMasjidPostList, (HomeMasjidPostList.class.getClassLoader()));
    }

    public HomeModelMy() {
    }

    public List<HomeMasjidPostList> getHomeMasjidPostList() {
        return homeMasjidPostList;
    }

    public void setHomeMasjidPostList(List<HomeMasjidPostList> homeMasjidPostList) {
        this.homeMasjidPostList = homeMasjidPostList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(homeMasjidPostList);
    }

    public int describeContents() {
        return 0;
    }

}