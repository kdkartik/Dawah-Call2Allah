package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowerListModel implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("verified_status")
    @Expose
    private String verifiedStatus;
    @SerializedName("pan_card_number")
    @Expose
    private String panCardNumber;
    @SerializedName("uidai_number")
    @Expose
    private String uidaiNumber;
    @SerializedName("front_photo_id")
    @Expose
    private String frontPhotoId;
    @SerializedName("back_photo_id")
    @Expose
    private String backPhotoId;
    @SerializedName("short_video")
    @Expose
    private String shortVideo;
    @SerializedName("pan_card_photo")
    @Expose
    private String panCardPhoto;
    @SerializedName("signature_photo")
    @Expose
    private String signaturePhoto;
    @SerializedName("user_avatar")
    @Expose
    private String userAvatar;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_token")
    @Expose
    private String userToken;
    @SerializedName("share_code")
    @Expose
    private String shareCode;
    @SerializedName("wallet_total")
    @Expose
    private String walletTotal;
    @SerializedName("follow_status")
    @Expose
    private Boolean followStatus;
    public final static Parcelable.Creator<FollowerListModel> CREATOR = new Creator<FollowerListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FollowerListModel createFromParcel(Parcel in) {
            return new FollowerListModel(in);
        }

        public FollowerListModel[] newArray(int size) {
            return (new FollowerListModel[size]);
        }

    };

    protected FollowerListModel(Parcel in) {
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.fullName = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.email = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.password = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.verifiedStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.panCardNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.uidaiNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.frontPhotoId = ((String) in.readValue((String.class.getClassLoader())));
        this.backPhotoId = ((String) in.readValue((String.class.getClassLoader())));
        this.shortVideo = ((String) in.readValue((String.class.getClassLoader())));
        this.panCardPhoto = ((String) in.readValue((String.class.getClassLoader())));
        this.signaturePhoto = ((String) in.readValue((String.class.getClassLoader())));
        this.userAvatar = ((String) in.readValue((String.class.getClassLoader())));
        this.createdOn = ((String) in.readValue((String.class.getClassLoader())));
        this.userToken = ((String) in.readValue((String.class.getClassLoader())));
        this.shareCode = ((String) in.readValue((String.class.getClassLoader())));
        this.walletTotal = ((String) in.readValue((String.class.getClassLoader())));
        this.followStatus = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public FollowerListModel() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(String verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public String getPanCardNumber() {
        return panCardNumber;
    }

    public void setPanCardNumber(String panCardNumber) {
        this.panCardNumber = panCardNumber;
    }

    public String getUidaiNumber() {
        return uidaiNumber;
    }

    public void setUidaiNumber(String uidaiNumber) {
        this.uidaiNumber = uidaiNumber;
    }

    public String getFrontPhotoId() {
        return frontPhotoId;
    }

    public void setFrontPhotoId(String frontPhotoId) {
        this.frontPhotoId = frontPhotoId;
    }

    public String getBackPhotoId() {
        return backPhotoId;
    }

    public void setBackPhotoId(String backPhotoId) {
        this.backPhotoId = backPhotoId;
    }

    public String getShortVideo() {
        return shortVideo;
    }

    public void setShortVideo(String shortVideo) {
        this.shortVideo = shortVideo;
    }

    public String getPanCardPhoto() {
        return panCardPhoto;
    }

    public void setPanCardPhoto(String panCardPhoto) {
        this.panCardPhoto = panCardPhoto;
    }

    public String getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(String signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getShareCode() {
        return shareCode;
    }

    public void setShareCode(String shareCode) {
        this.shareCode = shareCode;
    }

    public String getWalletTotal() {
        return walletTotal;
    }

    public void setWalletTotal(String walletTotal) {
        this.walletTotal = walletTotal;
    }

    public Boolean getFollowStatus() {
        return followStatus;
    }

    public void setFollowStatus(Boolean followStatus) {
        this.followStatus = followStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(fullName);
        dest.writeValue(phoneNumber);
        dest.writeValue(email);
        dest.writeValue(address);
        dest.writeValue(username);
        dest.writeValue(password);
        dest.writeValue(status);
        dest.writeValue(verifiedStatus);
        dest.writeValue(panCardNumber);
        dest.writeValue(uidaiNumber);
        dest.writeValue(frontPhotoId);
        dest.writeValue(backPhotoId);
        dest.writeValue(shortVideo);
        dest.writeValue(panCardPhoto);
        dest.writeValue(signaturePhoto);
        dest.writeValue(userAvatar);
        dest.writeValue(createdOn);
        dest.writeValue(userToken);
        dest.writeValue(shareCode);
        dest.writeValue(walletTotal);
        dest.writeValue(followStatus);
    }

    public int describeContents() {
        return 0;
    }
}