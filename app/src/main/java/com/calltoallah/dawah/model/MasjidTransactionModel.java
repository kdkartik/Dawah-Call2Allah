package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MasjidTransactionModel implements Parcelable {

    @SerializedName("masjid_transaction")
    @Expose
    private List<MasjidTransactionListModel> masjidTransactionList = null;
    public final static Parcelable.Creator<MasjidTransactionModel> CREATOR = new Creator<MasjidTransactionModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MasjidTransactionModel createFromParcel(Parcel in) {
            return new MasjidTransactionModel(in);
        }

        public MasjidTransactionModel[] newArray(int size) {
            return (new MasjidTransactionModel[size]);
        }

    };

    protected MasjidTransactionModel(Parcel in) {
        in.readList(this.masjidTransactionList, (MasjidTransactionListModel.class.getClassLoader()));
    }

    public MasjidTransactionModel() {
    }

    public List<MasjidTransactionListModel> getMasjidTransactionList() {
        return masjidTransactionList;
    }

    public void setMasjidTransactionList(List<MasjidTransactionListModel> masjidTransactionList) {
        this.masjidTransactionList = masjidTransactionList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(masjidTransactionList);
    }

    public int describeContents() {
        return 0;
    }

}