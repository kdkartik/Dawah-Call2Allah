package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("masjid_id")
    @Expose
    private String masjidId;
    @SerializedName("post_text")
    @Expose
    private String postText;
    @SerializedName("post_type")
    @Expose
    private String postType;
    @SerializedName("doc_path")
    @Expose
    private String docPath;
    @SerializedName("donation_button")
    @Expose
    private String donationButton;
    @SerializedName("total_counts")
    @Expose
    private String totalCounts;
    @SerializedName("asked_donation_amount")
    @Expose
    private String askedDonationAmount;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("masjid_name")
    @Expose
    private String masjidName;
    @SerializedName("masjid_username")
    @Expose
    private String masjidUsername;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("about_masjid")
    @Expose
    private String aboutMasjid;
    @SerializedName("masjid_image")
    @Expose
    private String masjidImage;
    @SerializedName("masjid_lat")
    @Expose
    private String masjidLat;
    @SerializedName("masjid_lng")
    @Expose
    private String masjidLng;
    @SerializedName("qr_code")
    @Expose
    private String qrCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("short_video")
    @Expose
    private String shortVideo;
    @SerializedName("pan_card_photo")
    @Expose
    private String panCardPhoto;
    @SerializedName("signature_photo")
    @Expose
    private String signaturePhoto;
    @SerializedName("masjid_total_amount")
    @Expose
    private String masjidTotalAmount;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("account_holder_name")
    @Expose
    private String accountHolderName;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("prayer_time")
    @Expose
    private String prayerTime;
    @SerializedName("prayer_name")
    @Expose
    private String prayerName;
    @SerializedName("total_students")
    @Expose
    private String totalStudents;
    @SerializedName("total_teachers")
    @Expose
    private String totalTeachers;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("verified_status")
    @Expose
    private String verifiedStatus;
    @SerializedName("like_count")
    @Expose
    private String likeCount;
    @SerializedName("share_count")
    @Expose
    private String shareCount;
    @SerializedName("user_donated")
    @Expose
    private String userDonated;
    @SerializedName("post_liked")
    @Expose
    private boolean post_liked;
    @SerializedName("masjid_username")
    @Expose
    private String masjid_username;

    public final static Parcelable.Creator<PostModel> CREATOR = new Creator<PostModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PostModel createFromParcel(Parcel in) {
            return new PostModel(in);
        }

        public PostModel[] newArray(int size) {
            return (new PostModel[size]);
        }

    };

    protected PostModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidId = ((String) in.readValue((String.class.getClassLoader())));
        this.postText = ((String) in.readValue((String.class.getClassLoader())));
        this.postType = ((String) in.readValue((String.class.getClassLoader())));
        this.docPath = ((String) in.readValue((String.class.getClassLoader())));
        this.donationButton = ((String) in.readValue((String.class.getClassLoader())));
        this.totalCounts = ((String) in.readValue((String.class.getClassLoader())));
        this.askedDonationAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.createdOn = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidName = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidUsername = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.aboutMasjid = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidImage = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidLat = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidLng = ((String) in.readValue((String.class.getClassLoader())));
        this.qrCode = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.shortVideo = ((String) in.readValue((String.class.getClassLoader())));
        this.panCardPhoto = ((String) in.readValue((String.class.getClassLoader())));
        this.signaturePhoto = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidTotalAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.bankName = ((String) in.readValue((String.class.getClassLoader())));
        this.accountHolderName = ((String) in.readValue((String.class.getClassLoader())));
        this.ifscCode = ((String) in.readValue((String.class.getClassLoader())));
        this.accountNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.prayerTime = ((String) in.readValue((String.class.getClassLoader())));
        this.prayerName = ((String) in.readValue((String.class.getClassLoader())));
        this.totalStudents = ((String) in.readValue((String.class.getClassLoader())));
        this.totalTeachers = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.verifiedStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.likeCount = ((String) in.readValue((String.class.getClassLoader())));
        this.shareCount = ((String) in.readValue((String.class.getClassLoader())));
        this.userDonated = ((String) in.readValue((String.class.getClassLoader())));
        this.post_liked = in.readByte() != 0;
        this.masjid_username = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PostModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMasjidId() {
        return masjidId;
    }

    public void setMasjidId(String masjidId) {
        this.masjidId = masjidId;
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }

    public String getDonationButton() {
        return donationButton;
    }

    public void setDonationButton(String donationButton) {
        this.donationButton = donationButton;
    }

    public String getTotalCounts() {
        return totalCounts;
    }

    public void setTotalCounts(String totalCounts) {
        this.totalCounts = totalCounts;
    }

    public String getAskedDonationAmount() {
        return askedDonationAmount;
    }

    public void setAskedDonationAmount(String askedDonationAmount) {
        this.askedDonationAmount = askedDonationAmount;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getMasjidUsername() {
        return masjidUsername;
    }

    public void setMasjidUsername(String masjidUsername) {
        this.masjidUsername = masjidUsername;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAboutMasjid() {
        return aboutMasjid;
    }

    public void setAboutMasjid(String aboutMasjid) {
        this.aboutMasjid = aboutMasjid;
    }

    public String getMasjidImage() {
        return masjidImage;
    }

    public void setMasjidImage(String imagePath) {
        this.masjidImage = imagePath;
    }

    public String getMasjidLat() {
        return masjidLat;
    }

    public void setMasjidLat(String masjidLat) {
        this.masjidLat = masjidLat;
    }

    public String getMasjidLng() {
        return masjidLng;
    }

    public void setMasjidLng(String masjidLng) {
        this.masjidLng = masjidLng;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShortVideo() {
        return shortVideo;
    }

    public void setShortVideo(String shortVideo) {
        this.shortVideo = shortVideo;
    }

    public String getPanCardPhoto() {
        return panCardPhoto;
    }

    public void setPanCardPhoto(String panCardPhoto) {
        this.panCardPhoto = panCardPhoto;
    }

    public String getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(String signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public String getMasjidTotalAmount() {
        return masjidTotalAmount;
    }

    public void setMasjidTotalAmount(String masjidTotalAmount) {
        this.masjidTotalAmount = masjidTotalAmount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPrayerTime() {
        return prayerTime;
    }

    public void setPrayerTime(String prayerTime) {
        this.prayerTime = prayerTime;
    }

    public String getPrayerName() {
        return prayerName;
    }

    public void setPrayerName(String prayerName) {
        this.prayerName = prayerName;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public void setTotalStudents(String totalStudents) {
        this.totalStudents = totalStudents;
    }

    public String getTotalTeachers() {
        return totalTeachers;
    }

    public void setTotalTeachers(String totalTeachers) {
        this.totalTeachers = totalTeachers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVerifiedStatus() {
        return verifiedStatus;
    }

    public void setVerifiedStatus(String verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getShareCount() {
        return shareCount;
    }

    public void setShareCount(String shareCount) {
        this.shareCount = shareCount;
    }

    public String getUserDonated() {
        return userDonated;
    }

    public void setUserDonated(String userDonated) {
        this.userDonated = userDonated;
    }

    public boolean isPost_liked() {
        return post_liked;
    }

    public void setPost_liked(boolean post_liked) {
        this.post_liked = post_liked;
    }

    public String getMasjid_username() {
        return masjid_username;
    }

    public void setMasjid_username(String masjid_username) {
        this.masjid_username = masjid_username;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(masjidId);
        dest.writeValue(postText);
        dest.writeValue(postType);
        dest.writeValue(docPath);
        dest.writeValue(donationButton);
        dest.writeValue(totalCounts);
        dest.writeValue(askedDonationAmount);
        dest.writeValue(createdOn);
        dest.writeValue(userId);
        dest.writeValue(masjidName);
        dest.writeValue(masjidUsername);
        dest.writeValue(registrationNumber);
        dest.writeValue(address);
        dest.writeValue(phoneNumber);
        dest.writeValue(aboutMasjid);
        dest.writeValue(masjidImage);
        dest.writeValue(masjidLat);
        dest.writeValue(masjidLng);
        dest.writeValue(qrCode);
        dest.writeValue(status);
        dest.writeValue(shortVideo);
        dest.writeValue(panCardPhoto);
        dest.writeValue(signaturePhoto);
        dest.writeValue(masjidTotalAmount);
        dest.writeValue(bankName);
        dest.writeValue(accountHolderName);
        dest.writeValue(ifscCode);
        dest.writeValue(accountNumber);
        dest.writeValue(prayerTime);
        dest.writeValue(prayerName);
        dest.writeValue(totalStudents);
        dest.writeValue(totalTeachers);
        dest.writeValue(type);
        dest.writeValue(verifiedStatus);
        dest.writeValue(likeCount);
        dest.writeValue(shareCount);
        dest.writeValue(userDonated);
        dest.writeByte((byte) (post_liked ? 1 : 0));
        dest.writeValue(masjid_username);
    }

    public int describeContents() {
        return 0;
    }



}