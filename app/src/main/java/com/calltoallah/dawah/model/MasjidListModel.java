package com.calltoallah.dawah.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasjidListModel extends RecyclerViewItem implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("masjid_name")
    @Expose
    private String masjidName;
    @SerializedName("masjid_username")
    @Expose
    private String masjidUsername;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phone_number")
    @Expose
    private String phoneNumber;
    @SerializedName("about_masjid")
    @Expose
    private String aboutMasjid;
    @SerializedName("image_path")
    @Expose
    private String imagePath;
    @SerializedName("masjid_lat")
    @Expose
    private String masjidLat;
    @SerializedName("masjid_lng")
    @Expose
    private String masjidLng;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("short_video")
    @Expose
    private String shortVideo;
    @SerializedName("pan_card_photo")
    @Expose
    private String panCardPhoto;
    @SerializedName("signature_photo")
    @Expose
    private String signaturePhoto;
    @SerializedName("masjid_total_amount")
    @Expose
    private String masjidTotalAmount;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("account_holder_name")
    @Expose
    private String accountHolderName;
    @SerializedName("ifsc_code")
    @Expose
    private String ifscCode;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;
    @SerializedName("prayer_time")
    @Expose
    private String prayerTime;
    @SerializedName("prayer_name")
    @Expose
    private String prayerName;
    @SerializedName("total_students")
    @Expose
    private String totalStudents;
    @SerializedName("total_teachers")
    @Expose
    private String totalTeachers;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("verified_status")
    @Expose
    private String verified_status;
    @SerializedName("follow_status")
    @Expose
    private int follow_status;

    @SerializedName("full_name")
    @Expose
    private String full_name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("user_avatar")
    @Expose
    private String user_avatar;
    @SerializedName("qr_code")
    @Expose
    private String qr_code;
    @SerializedName("post_count")
    @Expose
    private int post_count;
    @SerializedName("followers_count")
    @Expose
    private int followers_count;


    public final static Parcelable.Creator<MasjidListModel> CREATOR = new Creator<MasjidListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MasjidListModel createFromParcel(Parcel in) {
            return new MasjidListModel(in);
        }

        public MasjidListModel[] newArray(int size) {
            return (new MasjidListModel[size]);
        }

    };

    protected MasjidListModel(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidName = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidUsername = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationNumber = ((String) in.readValue((Object.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.aboutMasjid = ((String) in.readValue((String.class.getClassLoader())));
        this.imagePath = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidLat = ((String) in.readValue((String.class.getClassLoader())));
        this.masjidLng = ((String) in.readValue((String.class.getClassLoader())));
        this.createdOn = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.shortVideo = ((String) in.readValue((Object.class.getClassLoader())));
        this.panCardPhoto = ((String) in.readValue((Object.class.getClassLoader())));
        this.signaturePhoto = ((String) in.readValue((Object.class.getClassLoader())));
        this.masjidTotalAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.bankName = ((String) in.readValue((String.class.getClassLoader())));
        this.accountHolderName = ((String) in.readValue((String.class.getClassLoader())));
        this.ifscCode = ((String) in.readValue((String.class.getClassLoader())));
        this.accountNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.prayerTime = ((String) in.readValue((String.class.getClassLoader())));
        this.prayerName = ((String) in.readValue((String.class.getClassLoader())));
        this.totalStudents = ((String) in.readValue((String.class.getClassLoader())));
        this.totalTeachers = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.distance = ((String) in.readValue((String.class.getClassLoader())));
        this.verified_status = ((String) in.readValue((String.class.getClassLoader())));
        this.follow_status = ((Integer) in.readValue((String.class.getClassLoader())));
        this.full_name = ((String) in.readValue((String.class.getClassLoader())));
        this.username = ((String) in.readValue((String.class.getClassLoader())));
        this.user_avatar = ((String) in.readValue((String.class.getClassLoader())));
        this.qr_code = ((String) in.readValue((String.class.getClassLoader())));
        this.post_count = ((Integer) in.readValue((String.class.getClassLoader())));
        this.followers_count = ((Integer) in.readValue((String.class.getClassLoader())));
    }

    public MasjidListModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMasjidName() {
        return masjidName;
    }

    public void setMasjidName(String masjidName) {
        this.masjidName = masjidName;
    }

    public String getMasjidUsername() {
        return masjidUsername;
    }

    public void setMasjidUsername(String masjidUsername) {
        this.masjidUsername = masjidUsername;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAboutMasjid() {
        return aboutMasjid;
    }

    public void setAboutMasjid(String aboutMasjid) {
        this.aboutMasjid = aboutMasjid;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getMasjidLat() {
        return masjidLat;
    }

    public void setMasjidLat(String masjidLat) {
        this.masjidLat = masjidLat;
    }

    public String getMasjidLng() {
        return masjidLng;
    }

    public void setMasjidLng(String masjidLng) {
        this.masjidLng = masjidLng;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShortVideo() {
        return shortVideo;
    }

    public void setShortVideo(String shortVideo) {
        this.shortVideo = shortVideo;
    }

    public String getPanCardPhoto() {
        return panCardPhoto;
    }

    public void setPanCardPhoto(String panCardPhoto) {
        this.panCardPhoto = panCardPhoto;
    }

    public String getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(String signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public String getMasjidTotalAmount() {
        return masjidTotalAmount;
    }

    public void setMasjidTotalAmount(String masjidTotalAmount) {
        this.masjidTotalAmount = masjidTotalAmount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getPrayerTime() {
        return prayerTime;
    }

    public void setPrayerTime(String prayerTime) {
        this.prayerTime = prayerTime;
    }

    public String getPrayerName() {
        return prayerName;
    }

    public void setPrayerName(String prayerName) {
        this.prayerName = prayerName;
    }

    public String getTotalStudents() {
        return totalStudents;
    }

    public void setTotalStudents(String totalStudents) {
        this.totalStudents = totalStudents;
    }

    public String getTotalTeachers() {
        return totalTeachers;
    }

    public void setTotalTeachers(String totalTeachers) {
        this.totalTeachers = totalTeachers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getVerified_status() {
        return verified_status;
    }

    public void setVerified_status(String verified_status) {
        this.verified_status = verified_status;
    }

    public int getFollow_status() {
        return follow_status;
    }

    public void setFollow_status(int follow_status) {
        this.follow_status = follow_status;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    public int getPost_count() {
        return post_count;
    }

    public void setPost_count(int post_count) {
        this.post_count = post_count;
    }

    public int getFollowers_count() {
        return followers_count;
    }

    public void setFollowers_count(int followers_count) {
        this.followers_count = followers_count;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(userId);
        dest.writeValue(masjidName);
        dest.writeValue(masjidUsername);
        dest.writeValue(registrationNumber);
        dest.writeValue(address);
        dest.writeValue(phoneNumber);
        dest.writeValue(aboutMasjid);
        dest.writeValue(imagePath);
        dest.writeValue(masjidLat);
        dest.writeValue(masjidLng);
        dest.writeValue(createdOn);
        dest.writeValue(status);
        dest.writeValue(shortVideo);
        dest.writeValue(panCardPhoto);
        dest.writeValue(signaturePhoto);
        dest.writeValue(masjidTotalAmount);
        dest.writeValue(bankName);
        dest.writeValue(accountHolderName);
        dest.writeValue(ifscCode);
        dest.writeValue(accountNumber);
        dest.writeValue(prayerTime);
        dest.writeValue(prayerName);
        dest.writeValue(totalStudents);
        dest.writeValue(totalTeachers);
        dest.writeValue(type);
        dest.writeValue(distance);
        dest.writeValue(verified_status);
        dest.writeValue(follow_status);
        dest.writeValue(full_name);
        dest.writeValue(username);
        dest.writeValue(user_avatar);
        dest.writeValue(qr_code);
        dest.writeValue(post_count);
        dest.writeValue(followers_count);
    }

    public int describeContents() {
        return 0;
    }
}